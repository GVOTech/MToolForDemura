﻿namespace Demura_Analysis
{
    partial class V639DOE_U1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_sub = new System.Windows.Forms.ListBox();
            this.listBox_main = new System.Windows.Forms.ListBox();
            this.btn_load_main_csv = new System.Windows.Forms.Button();
            this.btn_CSV_Compose = new System.Windows.Forms.Button();
            this.btn_load_sub_csv = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.Percent0 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_Diff_Saveout = new System.Windows.Forms.Button();
            this.btn_Diff_Ref_Load = new System.Windows.Forms.Button();
            this.btn_Diff_Reset = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.G16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.G96 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.B16_Diff = new System.Windows.Forms.TextBox();
            this.B32_Diff = new System.Windows.Forms.TextBox();
            this.B64_Diff = new System.Windows.Forms.TextBox();
            this.B128_Diff = new System.Windows.Forms.TextBox();
            this.B192_Diff = new System.Windows.Forms.TextBox();
            this.B255_Diff = new System.Windows.Forms.TextBox();
            this.G16_Diff = new System.Windows.Forms.TextBox();
            this.G32_Diff = new System.Windows.Forms.TextBox();
            this.G64_Diff = new System.Windows.Forms.TextBox();
            this.G128_Diff = new System.Windows.Forms.TextBox();
            this.G192_Diff = new System.Windows.Forms.TextBox();
            this.G255_Diff = new System.Windows.Forms.TextBox();
            this.R16_Diff = new System.Windows.Forms.TextBox();
            this.R32_Diff = new System.Windows.Forms.TextBox();
            this.R64_Diff = new System.Windows.Forms.TextBox();
            this.R128_Diff = new System.Windows.Forms.TextBox();
            this.R192_Diff = new System.Windows.Forms.TextBox();
            this.R255_Diff = new System.Windows.Forms.TextBox();
            this.UDC_B = new System.Windows.Forms.TextBox();
            this.UDC_G = new System.Windows.Forms.TextBox();
            this.UDC_R = new System.Windows.Forms.TextBox();
            this.txt_path_sub = new System.Windows.Forms.TextBox();
            this.txt_path_main = new System.Windows.Forms.TextBox();
            this.Ref_G_639DOE = new System.Windows.Forms.Label();
            this.Ref_B_639DOE = new System.Windows.Forms.Label();
            this.Ref_R_639DOE = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox_sub
            // 
            this.listBox_sub.FormattingEnabled = true;
            this.listBox_sub.ItemHeight = 12;
            this.listBox_sub.Location = new System.Drawing.Point(170, 112);
            this.listBox_sub.Margin = new System.Windows.Forms.Padding(2);
            this.listBox_sub.Name = "listBox_sub";
            this.listBox_sub.Size = new System.Drawing.Size(138, 172);
            this.listBox_sub.TabIndex = 9;
            // 
            // listBox_main
            // 
            this.listBox_main.FormattingEnabled = true;
            this.listBox_main.ItemHeight = 12;
            this.listBox_main.Location = new System.Drawing.Point(11, 112);
            this.listBox_main.Margin = new System.Windows.Forms.Padding(2);
            this.listBox_main.Name = "listBox_main";
            this.listBox_main.Size = new System.Drawing.Size(138, 172);
            this.listBox_main.TabIndex = 8;
            // 
            // btn_load_main_csv
            // 
            this.btn_load_main_csv.Location = new System.Drawing.Point(37, 34);
            this.btn_load_main_csv.Name = "btn_load_main_csv";
            this.btn_load_main_csv.Size = new System.Drawing.Size(86, 29);
            this.btn_load_main_csv.TabIndex = 7;
            this.btn_load_main_csv.Text = "载入主屏CSV";
            this.btn_load_main_csv.UseVisualStyleBackColor = true;
            this.btn_load_main_csv.Click += new System.EventHandler(this.btn_load_main_csv_Click);
            // 
            // btn_CSV_Compose
            // 
            this.btn_CSV_Compose.Location = new System.Drawing.Point(367, 34);
            this.btn_CSV_Compose.Name = "btn_CSV_Compose";
            this.btn_CSV_Compose.Size = new System.Drawing.Size(102, 62);
            this.btn_CSV_Compose.TabIndex = 6;
            this.btn_CSV_Compose.Text = "CSV合成";
            this.btn_CSV_Compose.UseVisualStyleBackColor = true;
            this.btn_CSV_Compose.Click += new System.EventHandler(this.btn_CSV_Compose_Click);
            // 
            // btn_load_sub_csv
            // 
            this.btn_load_sub_csv.Location = new System.Drawing.Point(196, 34);
            this.btn_load_sub_csv.Name = "btn_load_sub_csv";
            this.btn_load_sub_csv.Size = new System.Drawing.Size(86, 29);
            this.btn_load_sub_csv.TabIndex = 5;
            this.btn_load_sub_csv.Text = "载入副屏CSV";
            this.btn_load_sub_csv.UseVisualStyleBackColor = true;
            this.btn_load_sub_csv.Click += new System.EventHandler(this.btn_load_sub_csv_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 327);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(296, 27);
            this.progressBar1.TabIndex = 10;
            // 
            // Percent0
            // 
            this.Percent0.AutoSize = true;
            this.Percent0.Location = new System.Drawing.Point(155, 334);
            this.Percent0.Name = "Percent0";
            this.Percent0.Size = new System.Drawing.Size(11, 12);
            this.Percent0.TabIndex = 11;
            this.Percent0.Text = "%";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_Diff_Saveout);
            this.groupBox2.Controls.Add(this.btn_Diff_Ref_Load);
            this.groupBox2.Controls.Add(this.btn_Diff_Reset);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.G16);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.G96);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.B16_Diff);
            this.groupBox2.Controls.Add(this.B32_Diff);
            this.groupBox2.Controls.Add(this.B64_Diff);
            this.groupBox2.Controls.Add(this.B128_Diff);
            this.groupBox2.Controls.Add(this.B192_Diff);
            this.groupBox2.Controls.Add(this.B255_Diff);
            this.groupBox2.Controls.Add(this.G16_Diff);
            this.groupBox2.Controls.Add(this.G32_Diff);
            this.groupBox2.Controls.Add(this.G64_Diff);
            this.groupBox2.Controls.Add(this.G128_Diff);
            this.groupBox2.Controls.Add(this.G192_Diff);
            this.groupBox2.Controls.Add(this.G255_Diff);
            this.groupBox2.Controls.Add(this.R16_Diff);
            this.groupBox2.Controls.Add(this.R32_Diff);
            this.groupBox2.Controls.Add(this.R64_Diff);
            this.groupBox2.Controls.Add(this.R128_Diff);
            this.groupBox2.Controls.Add(this.R192_Diff);
            this.groupBox2.Controls.Add(this.R255_Diff);
            this.groupBox2.Controls.Add(this.UDC_B);
            this.groupBox2.Controls.Add(this.UDC_G);
            this.groupBox2.Controls.Add(this.UDC_R);
            this.groupBox2.Location = new System.Drawing.Point(322, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(349, 180);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "校准系数";
            // 
            // btn_Diff_Saveout
            // 
            this.btn_Diff_Saveout.CausesValidation = false;
            this.btn_Diff_Saveout.Location = new System.Drawing.Point(219, 20);
            this.btn_Diff_Saveout.Name = "btn_Diff_Saveout";
            this.btn_Diff_Saveout.Size = new System.Drawing.Size(37, 23);
            this.btn_Diff_Saveout.TabIndex = 55;
            this.btn_Diff_Saveout.Text = "导出";
            this.btn_Diff_Saveout.UseVisualStyleBackColor = true;
            this.btn_Diff_Saveout.Click += new System.EventHandler(this.btn_Diff_Saveout_Click);
            // 
            // btn_Diff_Ref_Load
            // 
            this.btn_Diff_Ref_Load.Location = new System.Drawing.Point(262, 20);
            this.btn_Diff_Ref_Load.Name = "btn_Diff_Ref_Load";
            this.btn_Diff_Ref_Load.Size = new System.Drawing.Size(37, 23);
            this.btn_Diff_Ref_Load.TabIndex = 55;
            this.btn_Diff_Ref_Load.Text = "填参";
            this.btn_Diff_Ref_Load.UseVisualStyleBackColor = true;
            this.btn_Diff_Ref_Load.Click += new System.EventHandler(this.btn_Diff_Ref_Load_Click);
            // 
            // btn_Diff_Reset
            // 
            this.btn_Diff_Reset.Location = new System.Drawing.Point(304, 20);
            this.btn_Diff_Reset.Name = "btn_Diff_Reset";
            this.btn_Diff_Reset.Size = new System.Drawing.Size(37, 23);
            this.btn_Diff_Reset.TabIndex = 54;
            this.btn_Diff_Reset.Text = "重置";
            this.btn_Diff_Reset.UseVisualStyleBackColor = true;
            this.btn_Diff_Reset.Click += new System.EventHandler(this.btn_Diff_Reset_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label27.Location = new System.Drawing.Point(304, 136);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(23, 12);
            this.label27.TabIndex = 53;
            this.label27.Text = "B16";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(248, 136);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(23, 12);
            this.label28.TabIndex = 52;
            this.label28.Text = "B32";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label29.Location = new System.Drawing.Point(192, 136);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(23, 12);
            this.label29.TabIndex = 51;
            this.label29.Text = "B64";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label30.Location = new System.Drawing.Point(130, 136);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 50;
            this.label30.Text = "B128";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label32.Location = new System.Drawing.Point(68, 136);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 12);
            this.label32.TabIndex = 48;
            this.label32.Text = "B192";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label34.Location = new System.Drawing.Point(6, 136);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 46;
            this.label34.Text = "B255";
            // 
            // G16
            // 
            this.G16.AutoSize = true;
            this.G16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.G16.Location = new System.Drawing.Point(304, 96);
            this.G16.Name = "G16";
            this.G16.Size = new System.Drawing.Size(23, 12);
            this.G16.TabIndex = 45;
            this.G16.Text = "G16";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label20.Location = new System.Drawing.Point(248, 96);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 12);
            this.label20.TabIndex = 44;
            this.label20.Text = "G32";
            // 
            // G96
            // 
            this.G96.AutoSize = true;
            this.G96.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.G96.Location = new System.Drawing.Point(192, 96);
            this.G96.Name = "G96";
            this.G96.Size = new System.Drawing.Size(23, 12);
            this.G96.TabIndex = 43;
            this.G96.Text = "G64";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label22.Location = new System.Drawing.Point(130, 96);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 12);
            this.label22.TabIndex = 42;
            this.label22.Text = "G128";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label24.Location = new System.Drawing.Point(68, 96);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 12);
            this.label24.TabIndex = 40;
            this.label24.Text = "G192";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label26.Location = new System.Drawing.Point(6, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 12);
            this.label26.TabIndex = 38;
            this.label26.Text = "G255";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(303, 56);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 12);
            this.label18.TabIndex = 37;
            this.label18.Text = "R16";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(247, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 12);
            this.label17.TabIndex = 36;
            this.label17.Text = "R32";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(191, 56);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 12);
            this.label16.TabIndex = 35;
            this.label16.Text = "R64";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(129, 56);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 34;
            this.label15.Text = "R128";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(67, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "R192";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(5, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 30;
            this.label11.Text = "R255";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(94, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 12);
            this.label10.TabIndex = 29;
            this.label10.Text = "UDC_B";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 28;
            this.label9.Text = "UDC_G";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 27;
            this.label8.Text = "UDC_R";
            // 
            // B16_Diff
            // 
            this.B16_Diff.Location = new System.Drawing.Point(302, 151);
            this.B16_Diff.Name = "B16_Diff";
            this.B16_Diff.Size = new System.Drawing.Size(36, 21);
            this.B16_Diff.TabIndex = 26;
            this.B16_Diff.Text = "1.0";
            // 
            // B32_Diff
            // 
            this.B32_Diff.Location = new System.Drawing.Point(243, 151);
            this.B32_Diff.Name = "B32_Diff";
            this.B32_Diff.Size = new System.Drawing.Size(36, 21);
            this.B32_Diff.TabIndex = 25;
            this.B32_Diff.Text = "1.0";
            // 
            // B64_Diff
            // 
            this.B64_Diff.Location = new System.Drawing.Point(184, 151);
            this.B64_Diff.Name = "B64_Diff";
            this.B64_Diff.Size = new System.Drawing.Size(36, 21);
            this.B64_Diff.TabIndex = 24;
            this.B64_Diff.Text = "1.0";
            // 
            // B128_Diff
            // 
            this.B128_Diff.Location = new System.Drawing.Point(125, 151);
            this.B128_Diff.Name = "B128_Diff";
            this.B128_Diff.Size = new System.Drawing.Size(36, 21);
            this.B128_Diff.TabIndex = 23;
            this.B128_Diff.Text = "1.0";
            // 
            // B192_Diff
            // 
            this.B192_Diff.Location = new System.Drawing.Point(66, 151);
            this.B192_Diff.Name = "B192_Diff";
            this.B192_Diff.Size = new System.Drawing.Size(36, 21);
            this.B192_Diff.TabIndex = 21;
            this.B192_Diff.Text = "1.0";
            // 
            // B255_Diff
            // 
            this.B255_Diff.Location = new System.Drawing.Point(7, 151);
            this.B255_Diff.Name = "B255_Diff";
            this.B255_Diff.Size = new System.Drawing.Size(36, 21);
            this.B255_Diff.TabIndex = 19;
            this.B255_Diff.Text = "1.0";
            // 
            // G16_Diff
            // 
            this.G16_Diff.Location = new System.Drawing.Point(302, 110);
            this.G16_Diff.Name = "G16_Diff";
            this.G16_Diff.Size = new System.Drawing.Size(36, 21);
            this.G16_Diff.TabIndex = 18;
            this.G16_Diff.Text = "1.0";
            // 
            // G32_Diff
            // 
            this.G32_Diff.Location = new System.Drawing.Point(243, 110);
            this.G32_Diff.Name = "G32_Diff";
            this.G32_Diff.Size = new System.Drawing.Size(36, 21);
            this.G32_Diff.TabIndex = 17;
            this.G32_Diff.Text = "1.0";
            // 
            // G64_Diff
            // 
            this.G64_Diff.Location = new System.Drawing.Point(184, 110);
            this.G64_Diff.Name = "G64_Diff";
            this.G64_Diff.Size = new System.Drawing.Size(36, 21);
            this.G64_Diff.TabIndex = 16;
            this.G64_Diff.Text = "1.0";
            // 
            // G128_Diff
            // 
            this.G128_Diff.Location = new System.Drawing.Point(125, 110);
            this.G128_Diff.Name = "G128_Diff";
            this.G128_Diff.Size = new System.Drawing.Size(36, 21);
            this.G128_Diff.TabIndex = 15;
            this.G128_Diff.Text = "1.0";
            // 
            // G192_Diff
            // 
            this.G192_Diff.Location = new System.Drawing.Point(66, 110);
            this.G192_Diff.Name = "G192_Diff";
            this.G192_Diff.Size = new System.Drawing.Size(36, 21);
            this.G192_Diff.TabIndex = 13;
            this.G192_Diff.Text = "1.0";
            // 
            // G255_Diff
            // 
            this.G255_Diff.Location = new System.Drawing.Point(7, 110);
            this.G255_Diff.Name = "G255_Diff";
            this.G255_Diff.Size = new System.Drawing.Size(36, 21);
            this.G255_Diff.TabIndex = 11;
            this.G255_Diff.Text = "1.0";
            // 
            // R16_Diff
            // 
            this.R16_Diff.Location = new System.Drawing.Point(301, 70);
            this.R16_Diff.Name = "R16_Diff";
            this.R16_Diff.Size = new System.Drawing.Size(36, 21);
            this.R16_Diff.TabIndex = 10;
            this.R16_Diff.Text = "1.0";
            // 
            // R32_Diff
            // 
            this.R32_Diff.Location = new System.Drawing.Point(242, 70);
            this.R32_Diff.Name = "R32_Diff";
            this.R32_Diff.Size = new System.Drawing.Size(36, 21);
            this.R32_Diff.TabIndex = 9;
            this.R32_Diff.Text = "1.0";
            // 
            // R64_Diff
            // 
            this.R64_Diff.Location = new System.Drawing.Point(183, 70);
            this.R64_Diff.Name = "R64_Diff";
            this.R64_Diff.Size = new System.Drawing.Size(36, 21);
            this.R64_Diff.TabIndex = 8;
            this.R64_Diff.Text = "1.0";
            // 
            // R128_Diff
            // 
            this.R128_Diff.Location = new System.Drawing.Point(124, 70);
            this.R128_Diff.Name = "R128_Diff";
            this.R128_Diff.Size = new System.Drawing.Size(36, 21);
            this.R128_Diff.TabIndex = 7;
            this.R128_Diff.Text = "1.0";
            // 
            // R192_Diff
            // 
            this.R192_Diff.Location = new System.Drawing.Point(65, 70);
            this.R192_Diff.Name = "R192_Diff";
            this.R192_Diff.Size = new System.Drawing.Size(36, 21);
            this.R192_Diff.TabIndex = 5;
            this.R192_Diff.Text = "1.0";
            // 
            // R255_Diff
            // 
            this.R255_Diff.Location = new System.Drawing.Point(6, 70);
            this.R255_Diff.Name = "R255_Diff";
            this.R255_Diff.Size = new System.Drawing.Size(36, 21);
            this.R255_Diff.TabIndex = 3;
            this.R255_Diff.Text = "1.0";
            // 
            // UDC_B
            // 
            this.UDC_B.Location = new System.Drawing.Point(92, 30);
            this.UDC_B.Name = "UDC_B";
            this.UDC_B.Size = new System.Drawing.Size(36, 21);
            this.UDC_B.TabIndex = 2;
            this.UDC_B.Text = "1.0";
            // 
            // UDC_G
            // 
            this.UDC_G.Location = new System.Drawing.Point(49, 30);
            this.UDC_G.Name = "UDC_G";
            this.UDC_G.Size = new System.Drawing.Size(36, 21);
            this.UDC_G.TabIndex = 1;
            this.UDC_G.Text = "1.0";
            // 
            // UDC_R
            // 
            this.UDC_R.Location = new System.Drawing.Point(6, 30);
            this.UDC_R.Name = "UDC_R";
            this.UDC_R.Size = new System.Drawing.Size(36, 21);
            this.UDC_R.TabIndex = 0;
            this.UDC_R.Text = "1.0";
            // 
            // txt_path_sub
            // 
            this.txt_path_sub.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_path_sub.Location = new System.Drawing.Point(170, 73);
            this.txt_path_sub.Name = "txt_path_sub";
            this.txt_path_sub.Size = new System.Drawing.Size(138, 23);
            this.txt_path_sub.TabIndex = 31;
            // 
            // txt_path_main
            // 
            this.txt_path_main.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_path_main.Location = new System.Drawing.Point(12, 73);
            this.txt_path_main.Name = "txt_path_main";
            this.txt_path_main.Size = new System.Drawing.Size(137, 23);
            this.txt_path_main.TabIndex = 30;
            // 
            // Ref_G_639DOE
            // 
            this.Ref_G_639DOE.AutoSize = true;
            this.Ref_G_639DOE.Location = new System.Drawing.Point(9, 35);
            this.Ref_G_639DOE.Name = "Ref_G_639DOE";
            this.Ref_G_639DOE.Size = new System.Drawing.Size(17, 12);
            this.Ref_G_639DOE.TabIndex = 33;
            this.Ref_G_639DOE.Text = "-G";
            // 
            // Ref_B_639DOE
            // 
            this.Ref_B_639DOE.AutoSize = true;
            this.Ref_B_639DOE.Location = new System.Drawing.Point(9, 52);
            this.Ref_B_639DOE.Name = "Ref_B_639DOE";
            this.Ref_B_639DOE.Size = new System.Drawing.Size(17, 12);
            this.Ref_B_639DOE.TabIndex = 34;
            this.Ref_B_639DOE.Text = "-B";
            // 
            // Ref_R_639DOE
            // 
            this.Ref_R_639DOE.AutoSize = true;
            this.Ref_R_639DOE.Location = new System.Drawing.Point(9, 18);
            this.Ref_R_639DOE.Name = "Ref_R_639DOE";
            this.Ref_R_639DOE.Size = new System.Drawing.Size(17, 12);
            this.Ref_R_639DOE.TabIndex = 32;
            this.Ref_R_639DOE.Text = "-R";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Ref_R_639DOE);
            this.groupBox1.Controls.Add(this.Ref_B_639DOE);
            this.groupBox1.Controls.Add(this.Ref_G_639DOE);
            this.groupBox1.Location = new System.Drawing.Point(320, 290);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 75);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // V639DOE_U1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 382);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_path_sub);
            this.Controls.Add(this.txt_path_main);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Percent0);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.listBox_sub);
            this.Controls.Add(this.listBox_main);
            this.Controls.Add(this.btn_load_main_csv);
            this.Controls.Add(this.btn_CSV_Compose);
            this.Controls.Add(this.btn_load_sub_csv);
            this.Name = "V639DOE_U1";
            this.Text = "V639DOE_U1";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_sub;
        private System.Windows.Forms.ListBox listBox_main;
        private System.Windows.Forms.Button btn_load_main_csv;
        private System.Windows.Forms.Button btn_CSV_Compose;
        private System.Windows.Forms.Button btn_load_sub_csv;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label Percent0;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_Diff_Saveout;
        private System.Windows.Forms.Button btn_Diff_Ref_Load;
        private System.Windows.Forms.Button btn_Diff_Reset;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label G16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label G96;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox B16_Diff;
        private System.Windows.Forms.TextBox B32_Diff;
        private System.Windows.Forms.TextBox B64_Diff;
        private System.Windows.Forms.TextBox B128_Diff;
        private System.Windows.Forms.TextBox B192_Diff;
        private System.Windows.Forms.TextBox B255_Diff;
        private System.Windows.Forms.TextBox G16_Diff;
        private System.Windows.Forms.TextBox G32_Diff;
        private System.Windows.Forms.TextBox G64_Diff;
        private System.Windows.Forms.TextBox G128_Diff;
        private System.Windows.Forms.TextBox G192_Diff;
        private System.Windows.Forms.TextBox G255_Diff;
        private System.Windows.Forms.TextBox R16_Diff;
        private System.Windows.Forms.TextBox R32_Diff;
        private System.Windows.Forms.TextBox R64_Diff;
        private System.Windows.Forms.TextBox R128_Diff;
        private System.Windows.Forms.TextBox R192_Diff;
        private System.Windows.Forms.TextBox R255_Diff;
        private System.Windows.Forms.TextBox UDC_B;
        private System.Windows.Forms.TextBox UDC_G;
        private System.Windows.Forms.TextBox UDC_R;
        private System.Windows.Forms.TextBox txt_path_sub;
        private System.Windows.Forms.TextBox txt_path_main;
        private System.Windows.Forms.Label Ref_G_639DOE;
        private System.Windows.Forms.Label Ref_B_639DOE;
        private System.Windows.Forms.Label Ref_R_639DOE;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}