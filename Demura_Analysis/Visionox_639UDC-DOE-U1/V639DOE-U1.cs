﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Demura_Analysis
{
    public partial class V639DOE_U1 : Form
    {
        public V639DOE_U1()
        {
            InitializeComponent();
        }
        public string Default_Path = "";//暂存路径
        public string default0_filePath = "";//主屏CSV路径
        public string default1_filePath = "";//副屛CSV路径
        public double[] diff_R = new double[6];//保存主副屛选定区域平均值比值，R
        public double[] diff_G = new double[6];//保存主副屛选定区域平均值比值，R
        public double[] diff_B = new double[6];//保存主副屛选定区域平均值比值，R
        private Encoding encoding;        //编码
        int Buf_vactive = 2340;
        double averagezhu_sub = 0;//subCSV中主屏区域的平均值
        double averagezhu_main = 0;//mainCSV中主屏区域的平均值
        private void btn_load_main_csv_Click(object sender, EventArgs e)//载入主屏CSV
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.SelectedPath = Default_Path;
            if (path.ShowDialog() == DialogResult.OK)
            {
                txt_path_main.Text = path.SelectedPath;
                default0_filePath = path.SelectedPath;//将主屏CSV路径保存在该字符串下；
                //defaultfilePath = default0_filePath;
                Default_Path = path.SelectedPath;
                this.listBox_main.Items.Clear();
                string imgtype = "*.CSV";
                    string[] tmp1 = System.IO.Directory.GetFiles(path.SelectedPath, imgtype);
                    foreach (string s in tmp1)
                    {
                        listBox_main.Items.Add(new FileInfo(s).Name);
                    }
            }
            else
            {
                MessageBox.Show("没选择路径","系统提示");
            }
            
        }

        private void btn_load_sub_csv_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.SelectedPath = Default_Path;
            if (path.ShowDialog() == DialogResult.OK)
            {
                default1_filePath = path.SelectedPath;//保存副屛CSV路径
                Default_Path = path.SelectedPath;//更新暂存路径内容
                txt_path_sub.Text = path.SelectedPath;
                this.listBox_sub.Items.Clear();//清空listbox
                string imgtype = "*.CSV";//过滤文件类型
                string[] tmp1 = System.IO.Directory.GetFiles(path.SelectedPath, imgtype);//获取该路径下文件类型为CSV的所有集合
                foreach (string s in tmp1)
                {
                    listBox_sub.Items.Add(new FileInfo(s).Name);
                }
            }
            else
            {
                MessageBox.Show("没选择路径", "系统提示");
            }
                
        }

        private void btn_CSV_Compose_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox_main.Items.Count; i++)
            {
                listBox_main.SelectedIndex = i;//以主屏listbox中集合数量为循环数循环，每次移动光标选中
                listBox_sub.SelectedIndex = i;
                try
                {
                    string filePath0 = default0_filePath + "\\" + listBox_main.SelectedItem; //主屏
                    string filePath1 = default1_filePath + "\\" + listBox_sub.SelectedItem; //副屏
                    CSV_data_average_sub(filePath0, filePath1);
                    CSV_data_Compose(filePath0, filePath1);
                    Application.DoEvents();
                }
                catch
                {
                    MessageBox.Show("数据加载有误，请检查！");
                }
                if (listBox_sub.SelectedIndex > 0)
                {
                    progressBar1.Value = listBox_main.SelectedIndex * 100 / (listBox_main.Items.Count - 1);//进度条
                    Percent0.Text = progressBar1.Value.ToString() + "%";
                }
            }
            MessageBox.Show("数据合成完成");
            Ref_R_639DOE.Text = null;
            Ref_G_639DOE.Text = null;
            Ref_B_639DOE.Text = null;
            for (int a = 0; a < 6; a++)//将主副屛选定区域平均值比值打印出来
            {
                Ref_R_639DOE.Text = Ref_R_639DOE.Text + diff_R[a].ToString("0.000") +",";
                Ref_G_639DOE.Text = Ref_G_639DOE.Text + diff_G[a].ToString("0.000") + ",";
                Ref_B_639DOE.Text = Ref_B_639DOE.Text + diff_B[a].ToString("0.000") + ",";
            }
        }
        private void CSV_data_average_sub(string csvPath0, string csvPath1)//计算sub-CSV中，主屏一个固定区域的平均值
        {
            string[] bufzhu_sub = new string[] { };
            string[] bufzhu_main = new string[] { };
            string[] buf_line = new string[Buf_vactive];
            int count = 0;
            int vcount = 0;
            double str = 0;
            double str1 = 0;

            if (this.encoding == null)
            {
                this.encoding = Encoding.Default;
            }
            StreamReader sr_sub = new StreamReader(csvPath1, this.encoding); //主屏
            StreamReader sr_main = new StreamReader(csvPath0, this.encoding);
            string fileDataLine_sub;
            string fileDataLine_main;
            while (vcount < 80) 
            {
                fileDataLine_sub = sr_sub.ReadLine();//每次载入整个主屏CSV中的一行，如果这一行包含副屏区域，那么就替换为副屏数据，替换后的数据保存在buf_line里面
                fileDataLine_main = sr_sub.ReadLine();
                bufzhu_sub = fileDataLine_sub.Split(',');
                bufzhu_main = fileDataLine_main.Split(',');
                if (vcount > 70 && vcount < 75)
                {
                    for (int i = 349; i < 370; i++)
                    {
                        str = double.Parse(bufzhu_sub[i]) + str;
                        count++;
                        str1 = double.Parse(bufzhu_main[i]) + str1;
                    }
                }
                vcount++;

            }
            averagezhu_sub = str / count;
            averagezhu_main = str1 / count;
            Application.DoEvents();
        }
        private void CSV_data_Compose(string csvPath0, string csvPath1)//CSV合成
        {
            string[] buf_main = new string[] { };//用于保存一行main-CSV读入后的数据
            string[] buf_sub = new string[] { };//用于保存一行main-CSV读入后的数据
            string[] buf_line = new string[Buf_vactive];//用于合成完以后的最终数据集合，之后循环write
            string[,] CSV_data_main = new string[2340, 720];//保存每个载入的mainCSV数据
            string[,] CSV_data_sub = new string[2340, 720];//保存每个载入的subCSV数据
            double str = 1.0;//系数--每种颜色单色系数*每个灰阶的单个系数
            if (this.encoding == null)
            {
                this.encoding = Encoding.Default;
            }
            StreamReader sr_main = new StreamReader(csvPath0, this.encoding); //主屏
            StreamReader sr_sub = new StreamReader(csvPath1, this.encoding); //副屏
            string fileDataLine_Main;
            string fileDataLine_Sub;
            int vcount = 0;//计数，屏体V向分辨率
            switch (listBox_main.SelectedItem)
            {
                case "R16.csv":
                    str = (double.Parse(R16_Diff.Text)) * (double.Parse(UDC_R.Text));
                    break;
                case "G16.csv":
                    str = (double.Parse(G16_Diff.Text)) * (double.Parse(UDC_G.Text));
                    break;
                case "B16.csv":
                    str = (double.Parse(B16_Diff.Text)) * (double.Parse(UDC_B.Text));
                    break;
                case "R32.csv":
                    str = (double.Parse(R32_Diff.Text)) * (double.Parse(UDC_R.Text));
                    break;
                case "G32.csv":
                    str = (double.Parse(G32_Diff.Text)) * (double.Parse(UDC_G.Text));
                    break;
                case "B32.csv":
                    str = (double.Parse(B32_Diff.Text)) * (double.Parse(UDC_B.Text));
                    break;
                case "R64.csv":
                    str = (double.Parse(R64_Diff.Text)) * (double.Parse(UDC_R.Text));
                    break;
                case "G64.csv":
                    str = (double.Parse(G64_Diff.Text)) * (double.Parse(UDC_G.Text));
                    break;
                case "B64.csv":
                    str = (double.Parse(B64_Diff.Text)) * (double.Parse(UDC_B.Text));
                    break;
                case "R128.csv":
                    str = (double.Parse(R128_Diff.Text)) * (double.Parse(UDC_R.Text));
                    break;
                case "G128.csv":
                    str = (double.Parse(G128_Diff.Text)) * (double.Parse(UDC_G.Text));
                    break;
                case "B128.csv":
                    str = (double.Parse(B128_Diff.Text)) * (double.Parse(UDC_B.Text));
                    break;
                case "R192.csv":
                    str = (double.Parse(R192_Diff.Text)) * (double.Parse(UDC_R.Text));
                    break;
                case "G192.csv":
                    str = (double.Parse(G192_Diff.Text)) * (double.Parse(UDC_G.Text));
                    break;
                case "B192.csv":
                    str = (double.Parse(B192_Diff.Text)) * (double.Parse(UDC_B.Text));
                    break;
                case "R255.csv":
                    str = (double.Parse(R255_Diff.Text)) * (double.Parse(UDC_R.Text));
                    break;
                case "G255.csv":
                    str = (double.Parse(G255_Diff.Text)) * (double.Parse(UDC_G.Text));
                    break;
                case "B255.csv":
                    str = (double.Parse(B255_Diff.Text)) * (double.Parse(UDC_B.Text));
                    break;
                default:
                    MessageBox.Show(listBox_main.SelectedItem + "赋值不正确!"); break;
            }//根据当前载入的CSV名称，确定当前需要乘以的系数
            while (vcount < Buf_vactive) //2340 & 2460，
            {
                fileDataLine_Main = sr_main.ReadLine();//每次载入整个主屏CSV中的一行，如果这一行包含副屏区域，那么就替换为副屏数据，替换后的数据保存在buf_line里面
                fileDataLine_Sub = sr_sub.ReadLine();
                buf_main = fileDataLine_Main.Split(',');
                buf_sub = fileDataLine_Sub.Split(',');
                for (int i = 0; i < 720; i++)//在循环V向分辨率的基础上，再循环H向分辨率，将整个CSV中的数据写入二维数组
                {
                    CSV_data_main[vcount, i] = buf_main[i];
                    CSV_data_sub[vcount, i] = buf_sub[i];
                }
                if (vcount > 66) //仅替换0-67行
                {
                    buf_line[vcount] = fileDataLine_Main;//从67行开始，有效主屏区数据已保存在buf_line中了
                }
                vcount++;
            }
            sr_main.Close();
            sr_sub.Close();

            for (int y = 0; y < 67; y++)//循环V向67行，准备替换副屛区CSV
            {
                for (int i = 0; i < 720; i++)  //循环H向720列
                {
                    if (i > 293 && i < 428)//判断副屛有效数据的大概位置，进入后继续判断
                    {
                        if (double.Parse(CSV_data_sub[y, i]) > (averagezhu_sub * 2))//用副屛数据定位，如果副屛某点的值大于副屛中主屏部分平均值的1.6倍，则为副屛区有效像素点
                        {
                            CSV_data_main[y, i] = (Double.Parse(CSV_data_sub[y, i])).ToString();
                            CSV_data_main[y, i] = (double.Parse(CSV_data_main[y, i]) * str).ToString();
                        }
                        else if (double.Parse(CSV_data_sub[y, i]) < (averagezhu_sub * 0.6))//用副屛数据定位，如果副屛某点的值小于副屛中主屏部分平均值的0.6倍，则为副屛区无效像素点
                        {
                            if (y == 0)//如果循环在第一行像素，那么只取其周围5个点做判断
                            {
                                double[] a = { double.Parse(CSV_data_sub[y, i + 1]), double.Parse(CSV_data_sub[y + 1, i]), double.Parse(CSV_data_sub[y + 1, i + 1]), double.Parse(CSV_data_sub[y, i - 1]), double.Parse(CSV_data_sub[y + 1, i - 1]) };
                                Array.Sort(a);
                                if (a[4] > (averagezhu_sub*1.8))//如果其周围5个值中有一个大于平均值的数，即表示其周围有一个有效像素，将有效像素值填入该点
                                {
                                    CSV_data_main[y, i] = (a[4]).ToString();
                                    CSV_data_main[y, i] = ((double.Parse(CSV_data_main[y, i]))*str).ToString();
                                }
                                else
                                {
                                    CSV_data_main[y, i] = averagezhu_main.ToString();
                                    //MessageBox.Show(listBox_main.SelectedItem + y.ToString() + "行" + i.ToString() + "列" + "点填充了主屏的平均值");
                                };
                            }
                            else//如果循环不在整个数据的第一行，那么就取其9个点在内的8个数据，统计其中有几个有效像素点，平均后填入
                            {
                                double[] b = { double.Parse(CSV_data_sub[y, i + 1]), double.Parse(CSV_data_sub[y + 1, i]), double.Parse(CSV_data_sub[y + 1, i + 1]), double.Parse(CSV_data_sub[y, i - 1]), double.Parse(CSV_data_sub[y + 1, i - 1]), double.Parse(CSV_data_sub[y - 1, i]), double.Parse(CSV_data_sub[y - 1, i - 1]), double.Parse(CSV_data_sub[y - 1, i + 1]) };
                                Array.Sort(b);
                                if (b[6] > (averagezhu_sub*3))
                                {
                                    CSV_data_main[y, i] = ((b[6] + b[7]) / 2).ToString();
                                    CSV_data_main[y, i] = (double.Parse(CSV_data_main[y, i]) * str).ToString();
                                }
                                else if (b[7] > (averagezhu_sub*2))
                                {
                                    CSV_data_main[y, i] = (b[7]).ToString();
                                    CSV_data_main[y, i] = (double.Parse(CSV_data_main[y, i]) * str).ToString();
                                }
                                else
                                {
                                    CSV_data_main[y, i] = averagezhu_main.ToString();
                                    //MessageBox.Show(listBox_main.SelectedItem + y.ToString() + "行" + i.ToString() + "列" + "点填充了主屏的平均值");
                                };
                            }

                        }
                    }
                    buf_line[y] = buf_line[y] + CSV_data_main[y, i];
                    if (i < 719)  //719
                    {
                        buf_line[y] = buf_line[y] + ',';
                    }
                }
            }
            double average_main = 0;//合成后副屛区域的平均值
            double str1 = 0;//暂存副屛计数数据之和，准备计算平均值
            double diff_str = 0;
            int count1 = 0;
            for(int x = 20; x < 30; x++)
            {
                for(int z = 355; z < 365; z++)
                {
                    str1 = double.Parse(CSV_data_main[x, z]) + str1;
                    count1++;
                }
            }
            average_main = str1 / count1;
            diff_str = averagezhu_main / average_main;
            switch (listBox_main.SelectedItem)
            {
                case "R16.csv":
                    diff_R[5] = diff_str;
                    break;
                case "G16.csv":
                    diff_G[5] = diff_str;
                    break;
                case "B16.csv":
                    diff_B[5] = diff_str;
                    break;
                case "R32.csv":
                    diff_R[4] = diff_str;
                    break;
                case "G32.csv":
                    diff_G[4] = diff_str;
                    break;
                case "B32.csv":
                    diff_B[4] = diff_str;
                    break;
                case "R64.csv":
                    diff_R[3] = diff_str;
                    break;
                case "G64.csv":
                    diff_G[3] = diff_str;
                    break;
                case "B64.csv":
                    diff_B[3] = diff_str;
                    break;
                case "R128.csv":
                    diff_R[2] = diff_str;
                    break;
                case "G128.csv":
                    diff_G[2] = diff_str;
                    break;
                case "B128.csv":
                    diff_B[2] = diff_str;
                    break;
                case "R192.csv":
                    diff_R[1] = diff_str;
                    break;
                case "G192.csv":
                    diff_G[1] = diff_str;
                    break;
                case "B192.csv":
                    diff_B[1] = diff_str;
                    break;
                case "R255.csv":
                    diff_R[0] = diff_str;
                    break;
                case "G255.csv":
                    diff_G[0] = diff_str;
                    break;
                case "B255.csv":
                    diff_B[0] = diff_str;
                    break;
                default:
                    MessageBox.Show(listBox_main.SelectedItem + "存在非指定CSV名称"); break;

            }
                    //----------------------------主屏CSV写----------------------
                    StreamWriter sw = new StreamWriter(csvPath0, false, Encoding.Default);
            //System.IO.File.WriteAllLines(csvPath0, buf_line, Encoding.UTF8);
            for (int i = 0; i < Buf_vactive; i++)   //将buf_line中的数据写入sw中，sw代表保存的数据
            {
                sw.WriteLine(buf_line[i]);
            }
            sw.Close();
            Application.DoEvents();
        }

        private void btn_Diff_Ref_Load_Click(object sender, EventArgs e)//将主副屛比值系数填入
        {
            UDC_R.Text = "1.0"; UDC_G.Text = "1.0"; UDC_B.Text = "1.0";
            R255_Diff.Text = diff_R[0].ToString("0.000"); G255_Diff.Text = diff_G[0].ToString("0.000"); B255_Diff.Text = diff_B[0].ToString("0.000");
            R192_Diff.Text = diff_R[1].ToString("0.000"); G192_Diff.Text = diff_G[1].ToString("0.000"); B192_Diff.Text = diff_B[1].ToString("0.000");
            R128_Diff.Text = diff_R[2].ToString("0.000"); G128_Diff.Text = diff_G[2].ToString("0.000"); B128_Diff.Text = diff_B[2].ToString("0.000");
            R64_Diff.Text = diff_R[3].ToString("0.000"); G64_Diff.Text = diff_G[3].ToString("0.000"); B64_Diff.Text = diff_B[3].ToString("0.000");
            R32_Diff.Text = diff_R[4].ToString("0.000"); G32_Diff.Text = diff_G[4].ToString("0.000"); B32_Diff.Text = diff_B[4].ToString("0.000");
            R16_Diff.Text = diff_R[5].ToString("0.000"); G16_Diff.Text = diff_G[5].ToString("0.000"); B16_Diff.Text = diff_B[5].ToString("0.000");
        }

        private void btn_Diff_Reset_Click(object sender, EventArgs e)//重置主副屛比值系数为1
        {
            UDC_R.Text = "1.0"; UDC_G.Text = "1.0"; UDC_B.Text = "1.0";
            R255_Diff.Text = "1.0"; G255_Diff.Text = "1.0"; B255_Diff.Text = "1.0";
            R192_Diff.Text = "1.0"; G192_Diff.Text = "1.0"; B192_Diff.Text = "1.0";
            R128_Diff.Text = "1.0"; G128_Diff.Text = "1.0"; B128_Diff.Text = "1.0";
            R64_Diff.Text = "1.0"; G64_Diff.Text = "1.0"; B64_Diff.Text = "1.0";
            R32_Diff.Text = "1.0"; G32_Diff.Text = "1.0"; B32_Diff.Text = "1.0";
            R16_Diff.Text = "1.0"; G16_Diff.Text = "1.0"; B16_Diff.Text = "1.0";
        }

        private void btn_Diff_Saveout_Click(object sender, EventArgs e)
        {
            string Diff_string = null;
            Diff_string += "校准系数" + "\r\n";
            Diff_string += "UDC_R/G/B: " + UDC_R.Text + "," + UDC_G.Text + "," + UDC_B.Text + "\r\n";
            Diff_string += "R_line：" + R16_Diff.Text + "," + R32_Diff.Text + "," + R64_Diff.Text + "," + R128_Diff.Text + ","+ R192_Diff.Text + ","+ R255_Diff.Text + "\r\n";
            Diff_string += "G_line：" + G16_Diff.Text + "," + G32_Diff.Text + "," + G64_Diff.Text + "," + G128_Diff.Text + "," + G192_Diff.Text + "," + G255_Diff.Text + "\r\n";
            Diff_string += "B_line：" + B16_Diff.Text + "," + B32_Diff.Text + "," + B64_Diff.Text + "," + B128_Diff.Text + "," + B192_Diff.Text + "," + B255_Diff.Text + "\r\n";

            //创建保存对话框对象
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Code files (*.txt)|*.txt"; //过滤文件类型
            save.FilterIndex = 1;
            save.RestoreDirectory = true;
            if (save.ShowDialog() == DialogResult.OK)
            {
                string path = save.FileName;
                save.DefaultExt = ".txt";
                StreamWriter sw = new StreamWriter(path);
                sw.WriteLine(Diff_string);
                sw.Close();
            }
        }
    }
    
}
