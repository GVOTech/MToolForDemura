﻿namespace Demura_Analysis
{
    partial class Item
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Project_Choose = new System.Windows.Forms.Button();
            this.cBx_Item_Set = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Project_Choose
            // 
            this.btn_Project_Choose.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Project_Choose.Location = new System.Drawing.Point(223, 64);
            this.btn_Project_Choose.Name = "btn_Project_Choose";
            this.btn_Project_Choose.Size = new System.Drawing.Size(85, 50);
            this.btn_Project_Choose.TabIndex = 0;
            this.btn_Project_Choose.Text = "项目选择";
            this.btn_Project_Choose.UseVisualStyleBackColor = true;
            this.btn_Project_Choose.Click += new System.EventHandler(this.Btn_Project_Choose_Click);
            // 
            // cBx_Item_Set
            // 
            this.cBx_Item_Set.BackColor = System.Drawing.Color.White;
            this.cBx_Item_Set.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBx_Item_Set.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cBx_Item_Set.FormattingEnabled = true;
            this.cBx_Item_Set.Items.AddRange(new object[] {
            "692UDC",
            "639UDC",
            "639_DOE_U1"});
            this.cBx_Item_Set.Location = new System.Drawing.Point(12, 65);
            this.cBx_Item_Set.Name = "cBx_Item_Set";
            this.cBx_Item_Set.Size = new System.Drawing.Size(169, 33);
            this.cBx_Item_Set.TabIndex = 43;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Demura_Analysis.Properties.Resources._2020_06_03_2150331;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(-16, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(348, 48);
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 45;
            this.label1.Text = "V00.07";
            // 
            // Item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 127);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cBx_Item_Set);
            this.Controls.Add(this.btn_Project_Choose);
            this.Name = "Item";
            this.Text = "选型";
            this.Load += new System.EventHandler(this.Item_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Project_Choose;
        private System.Windows.Forms.ComboBox cBx_Item_Set;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}