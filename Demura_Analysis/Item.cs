﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demura_Analysis
{
    public partial class Item : Form
    {
        VUDC639 UDC_639 = new VUDC639();//兼容692
        V639DOE_U1 UDC_DOE_639 = new V639DOE_U1();
        public Item()
        {
            InitializeComponent();
        }

        private void Item_Load(object sender, EventArgs e)
        {

        }

        private void Btn_Project_Choose_Click(object sender, EventArgs e)
        {
            switch (cBx_Item_Set.SelectedIndex)
            {
                case 0:
                    UDC_639.Udc_type = 0;//692UDC
                    UDC_639.ShowDialog(); 
                    break;
                case 1:
                    UDC_639.Udc_type = 1;//639UDC
                    UDC_639.ShowDialog();
                    break;
                case 2:
                    UDC_DOE_639.ShowDialog();
                    break;
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
