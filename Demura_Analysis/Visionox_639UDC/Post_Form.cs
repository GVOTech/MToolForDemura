﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Demura_Analysis
{
    public partial class Post_Form : Form
    {
        private byte[] Demura_GrayScale_639UDC = new byte[] { 32, 64, 96, 128, 160, 192, 224, 255 }; //%目标拍摄灰阶 X轴
        public double[] Sub_Ave_639UDC_R = new double[8];
        public double[] Sub_Ave_639UDC_G = new double[8];
        public double[] Sub_Ave_639UDC_B = new double[8];
        public double[] Main_Ave_639UDC_R = new double[8];
        public double[] Main_Ave_639UDC_G = new double[8];
        public double[] Main_Ave_639UDC_B = new double[8];
        public Post_Form()
        {
            InitializeComponent();
        }

        private void Post_Form_Load(object sender, EventArgs e)
        {
            Post_Load_FormXml_Reader(null, null);
            //%绘制曲线R
            Post_chart1.Series[0].Points.DataBindXY(Demura_GrayScale_639UDC, Main_Ave_639UDC_R);
            Post_chart1.Series[1].Points.DataBindXY(Demura_GrayScale_639UDC, Sub_Ave_639UDC_R);
            //%绘制曲线G
            Post_chart2.Series[0].Points.DataBindXY(Demura_GrayScale_639UDC, Main_Ave_639UDC_G);
            Post_chart2.Series[1].Points.DataBindXY(Demura_GrayScale_639UDC, Sub_Ave_639UDC_G);
            //%绘制曲线B
            Post_chart3.Series[0].Points.DataBindXY(Demura_GrayScale_639UDC, Main_Ave_639UDC_B);
            Post_chart3.Series[1].Points.DataBindXY(Demura_GrayScale_639UDC, Sub_Ave_639UDC_B);
        }

        public void Post_Load_FormXml_Reader(object sender, EventArgs e)
        {
            string Path = $"{System.Environment.CurrentDirectory}" + "\\post@.xml";
            XmlTextReader reader = new XmlTextReader(Path);
            //List<BookModel> modelList = new List<BookModel>();
            //BookModel model = new BookModel();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        //------------------------------------------SSSS-------------
                        case "S_R032": Sub_Ave_639UDC_R[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R064": Sub_Ave_639UDC_R[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R096": Sub_Ave_639UDC_R[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R128": Sub_Ave_639UDC_R[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R160": Sub_Ave_639UDC_R[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R192": Sub_Ave_639UDC_R[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R224": Sub_Ave_639UDC_R[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_R255": Sub_Ave_639UDC_R[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //--------------------------------------
                        case "S_G032": Sub_Ave_639UDC_G[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G064": Sub_Ave_639UDC_G[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G096": Sub_Ave_639UDC_G[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G128": Sub_Ave_639UDC_G[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G160": Sub_Ave_639UDC_G[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G192": Sub_Ave_639UDC_G[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G224": Sub_Ave_639UDC_G[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_G255": Sub_Ave_639UDC_G[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //--------------------------------------
                        case "S_B032": Sub_Ave_639UDC_B[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B064": Sub_Ave_639UDC_B[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B096": Sub_Ave_639UDC_B[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B128": Sub_Ave_639UDC_B[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B160": Sub_Ave_639UDC_B[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B192": Sub_Ave_639UDC_B[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B224": Sub_Ave_639UDC_B[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "S_B255": Sub_Ave_639UDC_B[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //------------------------------------------MMMM-------------
                        case "M_R032": Main_Ave_639UDC_R[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R064": Main_Ave_639UDC_R[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R096": Main_Ave_639UDC_R[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R128": Main_Ave_639UDC_R[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R160": Main_Ave_639UDC_R[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R192": Main_Ave_639UDC_R[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R224": Main_Ave_639UDC_R[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_R255": Main_Ave_639UDC_R[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //--------------------------------------
                        case "M_G032": Main_Ave_639UDC_G[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G064": Main_Ave_639UDC_G[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G096": Main_Ave_639UDC_G[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G128": Main_Ave_639UDC_G[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G160": Main_Ave_639UDC_G[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G192": Main_Ave_639UDC_G[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G224": Main_Ave_639UDC_G[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_G255": Main_Ave_639UDC_G[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //--------------------------------------
                        case "M_B032": Main_Ave_639UDC_B[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B064": Main_Ave_639UDC_B[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B096": Main_Ave_639UDC_B[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B128": Main_Ave_639UDC_B[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B160": Main_Ave_639UDC_B[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B192": Main_Ave_639UDC_B[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B224": Main_Ave_639UDC_B[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "M_B255": Main_Ave_639UDC_B[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        default:; break;
                    }
                }
            }
            reader.Close();
        }

        public void Post_Load_FormXml_Update(object sender, EventArgs e)
        {
            string Path = $"{System.Environment.CurrentDirectory}" + "\\post@.xml";
            if (Sub_Ave_639UDC_R[7] == 0 || Sub_Ave_639UDC_R[7] == 0 || Sub_Ave_639UDC_R[7] == 0)
            {
                MessageBox.Show("无效数据!");
            }
            else
            {
                XmlTextWriter myXmlTextWriter = new XmlTextWriter(Path, null);
                //使用 Formatting 属性指定希望将 XML 设定为何种格式。 这样，子元素就可以通过使用 Indentation 和 IndentChar 属性来缩进。
                myXmlTextWriter.Formatting = Formatting.Indented;

                myXmlTextWriter.WriteStartDocument(false);
                myXmlTextWriter.WriteStartElement("datastore");

                myXmlTextWriter.WriteComment("记录主副区值的信息");
                myXmlTextWriter.WriteStartElement("data");

                myXmlTextWriter.WriteAttributeString("Type", "post");
                myXmlTextWriter.WriteAttributeString("ISBN", DateTime.Now.ToString());

                myXmlTextWriter.WriteElementString("S_R032", Sub_Ave_639UDC_R[0].ToString());
                myXmlTextWriter.WriteElementString("S_R064", Sub_Ave_639UDC_R[1].ToString());
                myXmlTextWriter.WriteElementString("S_R096", Sub_Ave_639UDC_R[2].ToString());
                myXmlTextWriter.WriteElementString("S_R128", Sub_Ave_639UDC_R[3].ToString());
                myXmlTextWriter.WriteElementString("S_R160", Sub_Ave_639UDC_R[4].ToString());
                myXmlTextWriter.WriteElementString("S_R192", Sub_Ave_639UDC_R[5].ToString());
                myXmlTextWriter.WriteElementString("S_R224", Sub_Ave_639UDC_R[6].ToString());
                myXmlTextWriter.WriteElementString("S_R255", Sub_Ave_639UDC_R[7].ToString());

                myXmlTextWriter.WriteElementString("S_G032", Sub_Ave_639UDC_G[0].ToString());
                myXmlTextWriter.WriteElementString("S_G064", Sub_Ave_639UDC_G[1].ToString());
                myXmlTextWriter.WriteElementString("S_G096", Sub_Ave_639UDC_G[2].ToString());
                myXmlTextWriter.WriteElementString("S_G128", Sub_Ave_639UDC_G[3].ToString());
                myXmlTextWriter.WriteElementString("S_G160", Sub_Ave_639UDC_G[4].ToString());
                myXmlTextWriter.WriteElementString("S_G192", Sub_Ave_639UDC_G[5].ToString());
                myXmlTextWriter.WriteElementString("S_G224", Sub_Ave_639UDC_G[6].ToString());
                myXmlTextWriter.WriteElementString("S_G255", Sub_Ave_639UDC_G[7].ToString());

                myXmlTextWriter.WriteElementString("S_B032", Sub_Ave_639UDC_B[0].ToString());
                myXmlTextWriter.WriteElementString("S_B064", Sub_Ave_639UDC_B[1].ToString());
                myXmlTextWriter.WriteElementString("S_B096", Sub_Ave_639UDC_B[2].ToString());
                myXmlTextWriter.WriteElementString("S_B128", Sub_Ave_639UDC_B[3].ToString());
                myXmlTextWriter.WriteElementString("S_B160", Sub_Ave_639UDC_B[4].ToString());
                myXmlTextWriter.WriteElementString("S_B192", Sub_Ave_639UDC_B[5].ToString());
                myXmlTextWriter.WriteElementString("S_B224", Sub_Ave_639UDC_B[6].ToString());
                myXmlTextWriter.WriteElementString("S_B255", Sub_Ave_639UDC_B[7].ToString());

                //----------------------
                myXmlTextWriter.WriteElementString("M_R032", Main_Ave_639UDC_R[0].ToString());
                myXmlTextWriter.WriteElementString("M_R064", Main_Ave_639UDC_R[1].ToString());
                myXmlTextWriter.WriteElementString("M_R096", Main_Ave_639UDC_R[2].ToString());
                myXmlTextWriter.WriteElementString("M_R128", Main_Ave_639UDC_R[3].ToString());
                myXmlTextWriter.WriteElementString("M_R160", Main_Ave_639UDC_R[4].ToString());
                myXmlTextWriter.WriteElementString("M_R192", Main_Ave_639UDC_R[5].ToString());
                myXmlTextWriter.WriteElementString("M_R224", Main_Ave_639UDC_R[6].ToString());
                myXmlTextWriter.WriteElementString("M_R255", Main_Ave_639UDC_R[7].ToString());

                myXmlTextWriter.WriteElementString("M_G032", Main_Ave_639UDC_G[0].ToString());
                myXmlTextWriter.WriteElementString("M_G064", Main_Ave_639UDC_G[1].ToString());
                myXmlTextWriter.WriteElementString("M_G096", Main_Ave_639UDC_G[2].ToString());
                myXmlTextWriter.WriteElementString("M_G128", Main_Ave_639UDC_G[3].ToString());
                myXmlTextWriter.WriteElementString("M_G160", Main_Ave_639UDC_G[4].ToString());
                myXmlTextWriter.WriteElementString("M_G192", Main_Ave_639UDC_G[5].ToString());
                myXmlTextWriter.WriteElementString("M_G224", Main_Ave_639UDC_G[6].ToString());
                myXmlTextWriter.WriteElementString("M_G255", Main_Ave_639UDC_G[7].ToString());

                myXmlTextWriter.WriteElementString("M_B032", Main_Ave_639UDC_B[0].ToString());
                myXmlTextWriter.WriteElementString("M_B064", Main_Ave_639UDC_B[1].ToString());
                myXmlTextWriter.WriteElementString("M_B096", Main_Ave_639UDC_B[2].ToString());
                myXmlTextWriter.WriteElementString("M_B128", Main_Ave_639UDC_B[3].ToString());
                myXmlTextWriter.WriteElementString("M_B160", Main_Ave_639UDC_B[4].ToString());
                myXmlTextWriter.WriteElementString("M_B192", Main_Ave_639UDC_B[5].ToString());
                myXmlTextWriter.WriteElementString("M_B224", Main_Ave_639UDC_B[6].ToString());
                myXmlTextWriter.WriteElementString("M_B255", Main_Ave_639UDC_B[7].ToString());

                myXmlTextWriter.WriteEndElement();
                myXmlTextWriter.WriteEndElement();

                myXmlTextWriter.Flush();
                myXmlTextWriter.Close();
            }
        }

    }
}
