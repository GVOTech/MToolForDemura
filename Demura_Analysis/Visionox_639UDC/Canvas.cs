﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Demura_Analysis
{
    public partial class Canvas : Form
    {
        //%%639标准值，大数据收集获得
        double[] Original_639UDC_R = new double[] {1.9077301357154821, 1.9528889148225177, 1.9718408429955465, 1.9511605176759517, 1.9515318305262672, 1.9591532167425578, 1.9951307031293095, 2.0209460455912405 };
        double[] Original_639UDC_G = new double[] {2.1790009886482102, 2.2110426942720571, 2.228043138311103, 2.2126319668232228, 2.2031471745569577, 2.2466873402478202, 2.2388318610007243, 2.290790145128792 };
        double[] Original_639UDC_B = new double[] {2.230057853733785, 2.2966557221725683, 2.2992147207041533, 2.2650507575516987, 2.3085178189378084, 2.2973510881512391, 2.3284936313115807, 2.2940089791914287 };

        public byte[] Demura_GrayScale = new byte[] { 32, 64, 96, 128, 160, 192, 224, 255 }; //%目标拍摄灰阶 X轴
        public double[] Diff_R = new double[8]; //%主副屏均值比值  Y轴
        public double[] Diff_G = new double[8];
        public double[] Diff_B = new double[8];

        public double[] Sample_R = new double[8]; //拍摄8个灰阶
        public double[] Sample_G = new double[8];
        public double[] Sample_B = new double[8];

        public double[] Ref_639UDC_R = new double[8]; //补偿系数
        public double[] Ref_639UDC_G = new double[8]; 
        public double[] Ref_639UDC_B = new double[8]; 
        public Canvas()
        {
            InitializeComponent();
        }

        private void Canvas_Load(object sender, EventArgs e)
        {
            Load_FormXml_Reader(null, null);
            //%绘制标准曲线
            chart1.Series[0].Points.DataBindXY(Demura_GrayScale, Sample_R);
            chart1.Series[1].Points.DataBindXY(Demura_GrayScale, Sample_G);
            chart1.Series[2].Points.DataBindXY(Demura_GrayScale, Sample_B);
            //%绘制CSV曲线
            chart1.Series[3].Points.DataBindXY(Demura_GrayScale, Diff_R);
            chart1.Series[4].Points.DataBindXY(Demura_GrayScale, Diff_G);
            chart1.Series[5].Points.DataBindXY(Demura_GrayScale, Diff_B);

            for (int i = 0; i < 8; i++)
            {
                Ref_639UDC_R[i] = (double)(Sample_R[i] / Diff_R[i]);
                Ref_639UDC_G[i] = (double)(Sample_G[i] / Diff_G[i]);
                Ref_639UDC_B[i] = (double)(Sample_B[i] / Diff_B[i]);
            }
        }
        public void Load_FormXml_Reader(object sender, EventArgs e)
        {
            string Path = $"{System.Environment.CurrentDirectory}" + "\\UDC.xml";
            XmlTextReader reader = new XmlTextReader(Path);
            //List<BookModel> modelList = new List<BookModel>();
            //BookModel model = new BookModel();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "R032": Sample_R[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R064": Sample_R[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R096": Sample_R[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R128": Sample_R[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R160": Sample_R[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R192": Sample_R[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R224": Sample_R[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "R255": Sample_R[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //--------------------------------------
                        case "G032": Sample_G[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G064": Sample_G[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G096": Sample_G[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G128": Sample_G[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G160": Sample_G[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G192": Sample_G[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G224": Sample_G[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "G255": Sample_G[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        //--------------------------------------
                        case "B032": Sample_B[0] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B064": Sample_B[1] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B096": Sample_B[2] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B128": Sample_B[3] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B160": Sample_B[4] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B192": Sample_B[5] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B224": Sample_B[6] = Double.Parse(reader.ReadElementString().Trim()); break;
                        case "B255": Sample_B[7] = Double.Parse(reader.ReadElementString().Trim()); break;
                        default:;break;
                    }
                }
            }
            reader.Close();
        }

        public void Load_FormXml_Update(object sender, EventArgs e)
        {
            string Path = $"{System.Environment.CurrentDirectory}" + "\\UDC.xml";
            if (Diff_R[7] == 0 || Diff_G[7] == 0 || Diff_B[7] == 0)
            {
                MessageBox.Show("无效数据!");
            }
            else
            {
                XmlTextWriter myXmlTextWriter = new XmlTextWriter(Path, null);
                //使用 Formatting 属性指定希望将 XML 设定为何种格式。 这样，子元素就可以通过使用 Indentation 和 IndentChar 属性来缩进。
                myXmlTextWriter.Formatting = Formatting.Indented;

                myXmlTextWriter.WriteStartDocument(false);
                myXmlTextWriter.WriteStartElement("datastore");

                myXmlTextWriter.WriteComment("记录基准值的信息");
                myXmlTextWriter.WriteStartElement("data");

                myXmlTextWriter.WriteAttributeString("Type", "xxxUDC");
                myXmlTextWriter.WriteAttributeString("ISBN", DateTime.Now.ToString());

                myXmlTextWriter.WriteElementString("R032", Diff_R[0].ToString());
                myXmlTextWriter.WriteElementString("R064", Diff_R[1].ToString());
                myXmlTextWriter.WriteElementString("R096", Diff_R[2].ToString());
                myXmlTextWriter.WriteElementString("R128", Diff_R[3].ToString());
                myXmlTextWriter.WriteElementString("R160", Diff_R[4].ToString());
                myXmlTextWriter.WriteElementString("R192", Diff_R[5].ToString());
                myXmlTextWriter.WriteElementString("R224", Diff_R[6].ToString());
                myXmlTextWriter.WriteElementString("R255", Diff_R[7].ToString());

                myXmlTextWriter.WriteElementString("G032", Diff_G[0].ToString());
                myXmlTextWriter.WriteElementString("G064", Diff_G[1].ToString());
                myXmlTextWriter.WriteElementString("G096", Diff_G[2].ToString());
                myXmlTextWriter.WriteElementString("G128", Diff_G[3].ToString());
                myXmlTextWriter.WriteElementString("G160", Diff_G[4].ToString());
                myXmlTextWriter.WriteElementString("G192", Diff_G[5].ToString());
                myXmlTextWriter.WriteElementString("G224", Diff_G[6].ToString());
                myXmlTextWriter.WriteElementString("G255", Diff_G[7].ToString());

                myXmlTextWriter.WriteElementString("B032", Diff_B[0].ToString());
                myXmlTextWriter.WriteElementString("B064", Diff_B[1].ToString());
                myXmlTextWriter.WriteElementString("B096", Diff_B[2].ToString());
                myXmlTextWriter.WriteElementString("B128", Diff_B[3].ToString());
                myXmlTextWriter.WriteElementString("B160", Diff_B[4].ToString());
                myXmlTextWriter.WriteElementString("B192", Diff_B[5].ToString());
                myXmlTextWriter.WriteElementString("B224", Diff_B[6].ToString());
                myXmlTextWriter.WriteElementString("B255", Diff_B[7].ToString());

                myXmlTextWriter.WriteEndElement();
                myXmlTextWriter.WriteEndElement();

                myXmlTextWriter.Flush();
                myXmlTextWriter.Close();
                MessageBox.Show("基准值-更新成功！");
            }
        }

        public void Load_FormXml_Return_to_Original(string r_sample_diff, string g_sample_diff , string b_sample_diff)
        {
            string Path = $"{System.Environment.CurrentDirectory}" + "\\UDC.xml";
            XmlTextWriter myXmlTextWriter = new XmlTextWriter(Path, null);
            //使用 Formatting 属性指定希望将 XML 设定为何种格式。 这样，子元素就可以通过使用 Indentation 和 IndentChar 属性来缩进。
            myXmlTextWriter.Formatting = Formatting.Indented;

            myXmlTextWriter.WriteStartDocument(false);
            myXmlTextWriter.WriteStartElement("datastore");

            myXmlTextWriter.WriteComment("记录基准值的信息");
            myXmlTextWriter.WriteStartElement("data");

            myXmlTextWriter.WriteAttributeString("Type", "xxxUDC");
            myXmlTextWriter.WriteAttributeString("ISBN", DateTime.Now.ToString());

            myXmlTextWriter.WriteElementString("R032", r_sample_diff);
            myXmlTextWriter.WriteElementString("R064", r_sample_diff);
            myXmlTextWriter.WriteElementString("R096", r_sample_diff);
            myXmlTextWriter.WriteElementString("R128", r_sample_diff);
            myXmlTextWriter.WriteElementString("R160", r_sample_diff);
            myXmlTextWriter.WriteElementString("R192", r_sample_diff);
            myXmlTextWriter.WriteElementString("R224", r_sample_diff);
            myXmlTextWriter.WriteElementString("R255", r_sample_diff);

            myXmlTextWriter.WriteElementString("G032", g_sample_diff);
            myXmlTextWriter.WriteElementString("G064", g_sample_diff);
            myXmlTextWriter.WriteElementString("G096", g_sample_diff);
            myXmlTextWriter.WriteElementString("G128", g_sample_diff);
            myXmlTextWriter.WriteElementString("G160", g_sample_diff);
            myXmlTextWriter.WriteElementString("G192", g_sample_diff);
            myXmlTextWriter.WriteElementString("G224", g_sample_diff);
            myXmlTextWriter.WriteElementString("G255", g_sample_diff);

            myXmlTextWriter.WriteElementString("B032", b_sample_diff);
            myXmlTextWriter.WriteElementString("B064", b_sample_diff);
            myXmlTextWriter.WriteElementString("B096", b_sample_diff);
            myXmlTextWriter.WriteElementString("B128", b_sample_diff);
            myXmlTextWriter.WriteElementString("B160", b_sample_diff);
            myXmlTextWriter.WriteElementString("B192", b_sample_diff);
            myXmlTextWriter.WriteElementString("B224", b_sample_diff);
            myXmlTextWriter.WriteElementString("B255", b_sample_diff);

            myXmlTextWriter.WriteEndElement();
            myXmlTextWriter.WriteEndElement();

            myXmlTextWriter.Flush();
            myXmlTextWriter.Close();
        }

        private void Canvas_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void Canvas_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }

        private void Chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
