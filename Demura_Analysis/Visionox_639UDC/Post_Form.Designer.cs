﻿namespace Demura_Analysis
{
    partial class Post_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Post_chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Post_chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Post_chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.Post_chart1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Post_chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Post_chart3)).BeginInit();
            this.SuspendLayout();
            // 
            // Post_chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.Post_chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Post_chart1.Legends.Add(legend1);
            this.Post_chart1.Location = new System.Drawing.Point(4, 6);
            this.Post_chart1.Name = "Post_chart1";
            series1.ChartArea = "ChartArea1";
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.Name = "主屏R";
            series2.ChartArea = "ChartArea1";
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            series2.Legend = "Legend1";
            series2.Name = "副屏R";
            this.Post_chart1.Series.Add(series1);
            this.Post_chart1.Series.Add(series2);
            this.Post_chart1.Size = new System.Drawing.Size(552, 333);
            this.Post_chart1.TabIndex = 0;
            this.Post_chart1.Text = "chart1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(570, 368);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Post_chart1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(562, 342);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Red";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Post_chart2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(562, 342);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Green";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.Post_chart3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(562, 342);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Blue";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Post_chart2
            // 
            chartArea2.Name = "ChartArea1";
            this.Post_chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.Post_chart2.Legends.Add(legend2);
            this.Post_chart2.Location = new System.Drawing.Point(4, 6);
            this.Post_chart2.Name = "Post_chart2";
            series3.ChartArea = "ChartArea1";
            series3.Color = System.Drawing.Color.Lime;
            series3.Legend = "Legend1";
            series3.Name = "主屏G";
            series4.ChartArea = "ChartArea1";
            series4.Color = System.Drawing.Color.Green;
            series4.Legend = "Legend1";
            series4.Name = "副屏G";
            this.Post_chart2.Series.Add(series3);
            this.Post_chart2.Series.Add(series4);
            this.Post_chart2.Size = new System.Drawing.Size(552, 333);
            this.Post_chart2.TabIndex = 1;
            this.Post_chart2.Text = "chart1";
            // 
            // Post_chart3
            // 
            chartArea3.Name = "ChartArea1";
            this.Post_chart3.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.Post_chart3.Legends.Add(legend3);
            this.Post_chart3.Location = new System.Drawing.Point(5, 5);
            this.Post_chart3.Name = "Post_chart3";
            series5.ChartArea = "ChartArea1";
            series5.Color = System.Drawing.Color.Blue;
            series5.Legend = "Legend1";
            series5.Name = "主屏B";
            series6.ChartArea = "ChartArea1";
            series6.Color = System.Drawing.Color.Navy;
            series6.Legend = "Legend1";
            series6.Name = "副屏B";
            this.Post_chart3.Series.Add(series5);
            this.Post_chart3.Series.Add(series6);
            this.Post_chart3.Size = new System.Drawing.Size(552, 333);
            this.Post_chart3.TabIndex = 2;
            this.Post_chart3.Text = "chart1";
            // 
            // Post_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 383);
            this.Controls.Add(this.tabControl1);
            this.Name = "Post_Form";
            this.Text = "Post_Form";
            this.Load += new System.EventHandler(this.Post_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Post_chart1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Post_chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Post_chart3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Post_chart1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart Post_chart2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart Post_chart3;
    }
}