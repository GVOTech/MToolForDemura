﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demura_Analysis
{
    public partial class Gamma : Form
    {
        public byte[] Gamma_GrayScale = new byte[] { 32, 64, 96, 128, 160, 192, 224, 255 }; //%目标拍摄灰阶 X轴
        public double[] Gamma_R = new double[8]; //%以255灰CSV数据为目标值的gamma2.2  Y轴
        public double[] Gamma_G = new double[8];
        public double[] Gamma_B = new double[8];

        int[] GrayScale = new int[256];
        double[] gamma2_2_r = new double[256];
        double[] gamma2_2_g = new double[256];
        double[] gamma2_2_b = new double[256];

        public Gamma()
        {
            InitializeComponent();
        }

        private void Gamma_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < GrayScale.Length; i++)
            {
                gamma2_2_r[i] = Math.Pow((double)(i / 255.0), 2.2) * Gamma_R[Gamma_R.Length - 1];
                gamma2_2_g[i] = Math.Pow((double)(i / 255.0), 2.2) * Gamma_G[Gamma_G.Length - 1];
                gamma2_2_b[i] = Math.Pow((double)(i / 255.0), 2.2) * Gamma_B[Gamma_B.Length - 1];
                GrayScale[i] = i;
            }

            //%绘制标准曲线
            chart1.Series[0].Points.DataBindXY(GrayScale, gamma2_2_r);
            chart1.Series[1].Points.DataBindXY(GrayScale, gamma2_2_g);
            chart1.Series[2].Points.DataBindXY(GrayScale, gamma2_2_b);
            //%绘制CSV曲线
            chart1.Series[3].Points.DataBindXY(Gamma_GrayScale, Gamma_R);
            chart1.Series[4].Points.DataBindXY(Gamma_GrayScale, Gamma_G);
            chart1.Series[5].Points.DataBindXY(Gamma_GrayScale, Gamma_B);
        }

        private void Gamma_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }
    }
}
