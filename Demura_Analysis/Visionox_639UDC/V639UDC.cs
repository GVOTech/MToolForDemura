﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demura_Analysis
{
    public partial class VUDC639 : Form
    {
        private Encoding encoding;        //编码
        private List<string> CSVType;
        public List<string> Line_Rowdata = new List<string>();//保存每个拍摄灰阶的整面的每一行CSV数据
        string[] Mura_Rowdata_Array; //保存每个拍摄灰阶的整面CSV数据
         
        int Num_of_grayscale = 8;  //拍摄灰阶的数量定义
        public double[,] Sub_GrayScale_Ave = new double[8, 3]; //保存副屏区CSV数据的均值
        public double[,] Main_GrayScale_Ave = new double[8, 3];//保存主屏区CSV数据的均值
        public string[] Gary_List = new string[] {"R32","G32","B32","R64","G64","B64","R96","G96","B96","R128","G128","B128","R160","G160","B160","R192","G192","B192","R224","G224","R224","R255","G255","B255"};
        Canvas Canvas_Form = new Canvas();
        Gamma Gamma_Form = new Gamma();
        Post_Form Post_Form0 = new Post_Form();
//-----------------------------------------
        public int Udc_type; //% 0->692UDC  1->639UDC
        int Buf_vactive = 0;
        int Buf_hactive = 0;
        int Buf_x0 = 0;
        int Buf_x1 = 0;
        int Buf_y0 = 0;
        int Buf_y1 = 0;
        string Path_639UDC = null;
//------------------------------------------
        string[] CapRas_032_R;
        string[] CapRas_032_G;
        string[] CapRas_032_B;
        string[] CapRas_064_R;
        string[] CapRas_064_G;
        string[] CapRas_064_B;
        string[] CapRas_096_R;
        string[] CapRas_096_G;
        string[] CapRas_096_B;
        string[] CapRas_128_R;
        string[] CapRas_128_G;
        string[] CapRas_128_B;
        string[] CapRas_160_R;
        string[] CapRas_160_G;
        string[] CapRas_160_B;
        string[] CapRas_192_R;
        string[] CapRas_192_G;
        string[] CapRas_192_B;
        string[] CapRas_224_R;
        string[] CapRas_224_G;
        string[] CapRas_224_B;  
        string[] CapRas_255_R;
        string[] CapRas_255_G;
        string[] CapRas_255_B;
                                    
        double[,] UDC_CapRas_032_R = new double[100, 100];
        double[,] UDC_CapRas_032_G = new double[100, 100];
        double[,] UDC_CapRas_032_B = new double[100, 100];

        double[,] UDC_CapRas_064_R = new double[100, 100];
        double[,] UDC_CapRas_064_G = new double[100, 100];
        double[,] UDC_CapRas_064_B = new double[100, 100];

        double[,] UDC_CapRas_096_R = new double[100, 100];
        double[,] UDC_CapRas_096_G = new double[100, 100];
        double[,] UDC_CapRas_096_B = new double[100, 100];
        double[,] UDC_CapRas_128_R = new double[100, 100];
        double[,] UDC_CapRas_128_G = new double[100, 100];
        double[,] UDC_CapRas_128_B = new double[100, 100];
        double[,] UDC_CapRas_160_R = new double[100, 100];
        double[,] UDC_CapRas_160_G = new double[100, 100];
        double[,] UDC_CapRas_160_B = new double[100, 100];
        double[,] UDC_CapRas_192_R = new double[100, 100];
        double[,] UDC_CapRas_192_G = new double[100, 100];
        double[,] UDC_CapRas_192_B = new double[100, 100];
        double[,] UDC_CapRas_224_R = new double[100, 100];
        double[,] UDC_CapRas_224_G = new double[100, 100];
        double[,] UDC_CapRas_224_B = new double[100, 100];
        double[,] UDC_CapRas_255_R = new double[100, 100];
        double[,] UDC_CapRas_255_G = new double[100, 100];
        double[,] UDC_CapRas_255_B = new double[100, 100];

        double[,] Main_CapRas_032_R = new double[100, 100];
        double[,] Main_CapRas_032_G = new double[100, 100];
        double[,] Main_CapRas_032_B = new double[100, 100];
        double[,] Main_CapRas_064_R = new double[100, 100];
        double[,] Main_CapRas_064_G = new double[100, 100];
        double[,] Main_CapRas_064_B = new double[100, 100];
        double[,] Main_CapRas_096_R = new double[100, 100];
        double[,] Main_CapRas_096_G = new double[100, 100];
        double[,] Main_CapRas_096_B = new double[100, 100];
        double[,] Main_CapRas_128_R = new double[100, 100];
        double[,] Main_CapRas_128_G = new double[100, 100];
        double[,] Main_CapRas_128_B = new double[100, 100];
        double[,] Main_CapRas_160_R = new double[100, 100];
        double[,] Main_CapRas_160_G = new double[100, 100];
        double[,] Main_CapRas_160_B = new double[100, 100];
        double[,] Main_CapRas_192_R = new double[100, 100];
        double[,] Main_CapRas_192_G = new double[100, 100];
        double[,] Main_CapRas_192_B = new double[100, 100];
        double[,] Main_CapRas_224_R = new double[100, 100];
        double[,] Main_CapRas_224_G = new double[100, 100];
        double[,] Main_CapRas_224_B = new double[100, 100];
        double[,] Main_CapRas_255_R = new double[100, 100];
        double[,] Main_CapRas_255_G = new double[100, 100];
        double[,] Main_CapRas_255_B = new double[100, 100];
        public VUDC639()
        {
            InitializeComponent();
        }

        // <summary>
        // 初始化692UDC&639UDC 副屏区Region配置
        // </summary>
        private void Demura_Analysis_Load(object sender, EventArgs e)
        { //%%定义副屏区region
            if (Udc_type == 0)//%692UDC
            {
                tBx_UDC_x0.Text = "337";
                tBx_UDC_x1.Text = "384";
                tBx_UDC_y0.Text = "1";
                tBx_UDC_y1.Text = "80";
                tBx_Hactive.Text = "720";
                tBx_Vactive.Text = "2460";
                label_model.Text = "692UDC";
            }
            else if (Udc_type == 1)//%639UDC
            {
                tBx_UDC_x0.Text = "337";
                tBx_UDC_x1.Text = "384";
                tBx_UDC_y0.Text = "1";
                tBx_UDC_y1.Text = "64";
                tBx_Hactive.Text = "720";
                tBx_Vactive.Text = "2340";
                label_model.Text = "639UDC";
            }
            Buf_vactive = UInt16.Parse(tBx_Vactive.Text);
            Buf_hactive = UInt16.Parse(tBx_Hactive.Text);
            Buf_x0 = UInt16.Parse(tBx_UDC_x0.Text);
            Buf_x1 = UInt16.Parse(tBx_UDC_x1.Text);
            Buf_y0 = UInt16.Parse(tBx_UDC_y0.Text);
            Buf_y1 = UInt16.Parse(tBx_UDC_y1.Text);
        }

        /// <summary>
        /// 载入主副屏合成后的CSV路径
        /// </summary>
        public string defaultfilePath = "";
        public string Default_Path="";
        private void Btn_load_csv_path_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.SelectedPath = defaultfilePath;
            path.ShowDialog();
            CSV_Data_Path.Text = path.SelectedPath;
            defaultfilePath = path.SelectedPath;
            Default_Path = path.SelectedPath;

            this.listBox_CSVFile.Items.Clear();
            string imgtype = "*.CSV";
            string[] ImageType = imgtype.Split('|');
            for (int i = 0; i < ImageType.Length; i++)
            {
                string[] tmp1 = System.IO.Directory.GetFiles(path.SelectedPath, ImageType[i]);
                foreach (string s in tmp1)
                {
                    listBox_CSVFile.Items.Add(new FileInfo(s).Name);
                }
            }
        }
        /// <summary>
        /// 载入主副屏合成后的CSV路径，实际数据载入
        /// </summary>
        private void Btn_Load_CSV_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox_CSVFile.Items.Count; i++)
            {
                listBox_CSVFile.SelectedIndex = i;
                string filePath = defaultfilePath + "\\" + listBox_CSVFile.SelectedItem;
                CsvStreamReader(filePath);

                progressBar1.Value = listBox_CSVFile.SelectedIndex * 100 / (listBox_CSVFile.Items.Count - 1);
                Percent.Text = progressBar1.Value.ToString() + "%";
                Application.DoEvents();
            }
        }
        /// <summary>
        /// 计算并生成主副屏离散曲线
        /// </summary>
        private void Btn_CSV_Convert_to_offset_Click(object sender, EventArgs e)
        {
            try
            {
                UDC_Region_Data_Convert(null, null);//计算副屏区均值
                Main_Region_Data_Convert(null, null);//计算主屏区局部均值
                Diff_Cal_of_UDC_With_Main(null, null); //计算主副屏离散程度
            }
            catch
            {
                MessageBox.Show("CSV数据未加载!");
            }
            Canvas_Form.ShowDialog();//show主副屏离散曲线
            Btn_CSV_Cal_to_Ref(null, null); //打印理想系数
        }
        public void CsvStreamReader(string fileName)
        {
            // this.encoding = Encoding.Default;
            LoadCsvFile(fileName);
        }
        /// <summary>
        /// 载入CSV文件
        /// </summary>
        private void LoadCsvFile(string fileName)
        {
            //对数据的有效性进行验证
            if (fileName == null)
            {
                throw new Exception("请指定要载入的CSV文件名");
            }
            else if (!File.Exists(fileName))
            {
                throw new Exception("指定的CSV文件不存在");
            }
            else
            {
            }
            if (this.encoding == null)
            {
                this.encoding = Encoding.Default;
            }
            StreamReader sr = new StreamReader(fileName, this.encoding);
            string csvDataLine;
            csvDataLine = "";
            while (true)
            {
                string fileDataLine;
                fileDataLine = sr.ReadLine();
                if (fileDataLine == null)
                {
                    break;
                }
                else
                {
                    Line_Rowdata.Add(fileDataLine);//循环添加元素
                }
            }
            sr.Close();
            Mura_Rowdata_Array = Line_Rowdata.ToArray();//strArray=[str0,str1,str2]
            Line_Rowdata.Clear();//清空buffer
            if (Mura_Rowdata_Array.Length != Buf_vactive)//若数据量不正确 =2340 & 2460
            {
                MessageBox.Show(listBox_CSVFile.SelectedItem+"数据加载不正确!");
            }
            switch(listBox_CSVFile.SelectedItem)
            {
                case "CapRas_032_R.CSV": CapRas_032_R = Mura_Rowdata_Array; break;
                case "CapRas_032_G.CSV": CapRas_032_G = Mura_Rowdata_Array; break;
                case "CapRas_032_B.CSV": CapRas_032_B = Mura_Rowdata_Array; break;
                case "CapRas_064_R.CSV": CapRas_064_R = Mura_Rowdata_Array; break;
                case "CapRas_064_G.CSV": CapRas_064_G = Mura_Rowdata_Array; break;
                case "CapRas_064_B.CSV": CapRas_064_B = Mura_Rowdata_Array; break;
                case "CapRas_096_R.CSV": CapRas_096_R = Mura_Rowdata_Array; break;
                case "CapRas_096_G.CSV": CapRas_096_G = Mura_Rowdata_Array; break;
                case "CapRas_096_B.CSV": CapRas_096_B = Mura_Rowdata_Array; break;
                case "CapRas_128_R.CSV": CapRas_128_R = Mura_Rowdata_Array; break;
                case "CapRas_128_G.CSV": CapRas_128_G = Mura_Rowdata_Array; break;
                case "CapRas_128_B.CSV": CapRas_128_B = Mura_Rowdata_Array; break;
                case "CapRas_160_R.CSV": CapRas_160_R = Mura_Rowdata_Array; break;
                case "CapRas_160_G.CSV": CapRas_160_G = Mura_Rowdata_Array; break;
                case "CapRas_160_B.CSV": CapRas_160_B = Mura_Rowdata_Array; break;
                case "CapRas_192_R.CSV": CapRas_192_R = Mura_Rowdata_Array; break;
                case "CapRas_192_G.CSV": CapRas_192_G = Mura_Rowdata_Array; break;
                case "CapRas_192_B.CSV": CapRas_192_B = Mura_Rowdata_Array; break;
                case "CapRas_224_R.CSV": CapRas_224_R = Mura_Rowdata_Array; break;
                case "CapRas_224_G.CSV": CapRas_224_G = Mura_Rowdata_Array; break;
                case "CapRas_224_B.CSV": CapRas_224_B = Mura_Rowdata_Array; break;
                case "CapRas_255_R.CSV": CapRas_255_R = Mura_Rowdata_Array; break;
                case "CapRas_255_G.CSV": CapRas_255_G = Mura_Rowdata_Array; break;
                case "CapRas_255_B.CSV": CapRas_255_B = Mura_Rowdata_Array; break;


                case "CapRas_032_R.csv": CapRas_032_R = Mura_Rowdata_Array; break;
                case "CapRas_032_G.csv": CapRas_032_G = Mura_Rowdata_Array; break;
                case "CapRas_032_B.csv": CapRas_032_B = Mura_Rowdata_Array; break;
                case "CapRas_064_R.csv": CapRas_064_R = Mura_Rowdata_Array; break;
                case "CapRas_064_G.csv": CapRas_064_G = Mura_Rowdata_Array; break;
                case "CapRas_064_B.csv": CapRas_064_B = Mura_Rowdata_Array; break;
                case "CapRas_096_R.csv": CapRas_096_R = Mura_Rowdata_Array; break;
                case "CapRas_096_G.csv": CapRas_096_G = Mura_Rowdata_Array; break;
                case "CapRas_096_B.csv": CapRas_096_B = Mura_Rowdata_Array; break;
                case "CapRas_128_R.csv": CapRas_128_R = Mura_Rowdata_Array; break;
                case "CapRas_128_G.csv": CapRas_128_G = Mura_Rowdata_Array; break;
                case "CapRas_128_B.csv": CapRas_128_B = Mura_Rowdata_Array; break;
                case "CapRas_160_R.csv": CapRas_160_R = Mura_Rowdata_Array; break;
                case "CapRas_160_G.csv": CapRas_160_G = Mura_Rowdata_Array; break;
                case "CapRas_160_B.csv": CapRas_160_B = Mura_Rowdata_Array; break;
                case "CapRas_192_R.csv": CapRas_192_R = Mura_Rowdata_Array; break;
                case "CapRas_192_G.csv": CapRas_192_G = Mura_Rowdata_Array; break;
                case "CapRas_192_B.csv": CapRas_192_B = Mura_Rowdata_Array; break;
                case "CapRas_224_R.csv": CapRas_224_R = Mura_Rowdata_Array; break;
                case "CapRas_224_G.csv": CapRas_224_G = Mura_Rowdata_Array; break;
                case "CapRas_224_B.csv": CapRas_224_B = Mura_Rowdata_Array; break;
                case "CapRas_255_R.csv": CapRas_255_R = Mura_Rowdata_Array; break;
                case "CapRas_255_G.csv": CapRas_255_G = Mura_Rowdata_Array; break;
                case "CapRas_255_B.csv": CapRas_255_B = Mura_Rowdata_Array; break;

                case "R32.csv": CapRas_032_R = Mura_Rowdata_Array; break;
                case "G32.csv": CapRas_032_G = Mura_Rowdata_Array; break;
                case "B32.csv": CapRas_032_B = Mura_Rowdata_Array; break;
                case "R64.csv": CapRas_064_R = Mura_Rowdata_Array; break;
                case "G64.csv": CapRas_064_G = Mura_Rowdata_Array; break;
                case "B64.csv": CapRas_064_B = Mura_Rowdata_Array; break;
                case "R96.csv": CapRas_096_R = Mura_Rowdata_Array; break;
                case "G96.csv": CapRas_096_G = Mura_Rowdata_Array; break;
                case "B96.csv": CapRas_096_B = Mura_Rowdata_Array; break;
                case "R128.csv": CapRas_128_R = Mura_Rowdata_Array; break;
                case "G128.csv": CapRas_128_G = Mura_Rowdata_Array; break;
                case "B128.csv": CapRas_128_B = Mura_Rowdata_Array; break;
                case "R160.csv": CapRas_160_R = Mura_Rowdata_Array; break;
                case "G160.csv": CapRas_160_G = Mura_Rowdata_Array; break;
                case "B160.csv": CapRas_160_B = Mura_Rowdata_Array; break;
                case "R192.csv": CapRas_192_R = Mura_Rowdata_Array; break;
                case "G192.csv": CapRas_192_G = Mura_Rowdata_Array; break;
                case "B192.csv": CapRas_192_B = Mura_Rowdata_Array; break;
                case "R224.csv": CapRas_224_R = Mura_Rowdata_Array; break;
                case "G224.csv": CapRas_224_G = Mura_Rowdata_Array; break;
                case "B224.csv": CapRas_224_B = Mura_Rowdata_Array; break;
                case "R255.csv": CapRas_255_R = Mura_Rowdata_Array; break;
                case "G255.csv": CapRas_255_G = Mura_Rowdata_Array; break;
                case "B255.csv": CapRas_255_B = Mura_Rowdata_Array; break;

                default: MessageBox.Show(listBox_CSVFile.SelectedItem + "CapRas赋值不正确!"); break;
            }
        }

//-----------------------------------------------------------------
        private void UDC_Region_Data_Convert(object sender, EventArgs e)
        {
            string[] arr_r = new string[] { };
            string[] arr_g = new string[] { };
            string[] arr_b = new string[] { };
            int udc_area_x = Buf_x1 - Buf_x0 + 1; //%%UDC 范围设定
            int udc_area_y = Buf_y1 - Buf_y0 + 1;

            UDC_CapRas_032_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_032_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_032_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_064_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_064_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_064_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_096_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_096_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_096_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_128_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_128_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_128_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_160_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_160_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_160_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_192_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_192_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_192_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_224_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_224_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_224_B = new double[udc_area_y, udc_area_x];

            UDC_CapRas_255_R = new double[udc_area_y, udc_area_x];
            UDC_CapRas_255_G = new double[udc_area_y, udc_area_x];
            UDC_CapRas_255_B = new double[udc_area_y, udc_area_x];

            //提取各副屏区矩阵
            for (int g = 0; g < Canvas_Form.Demura_GrayScale.Length; g++)  //=8
            {   //提取副屏区矩阵
                for (int y = Buf_y0 - 1; y < Buf_y1; y++) //逐行提取
                {
                    switch (Canvas_Form.Demura_GrayScale[g])
                    {
                        case 32:
                            arr_r = CapRas_032_R[y].Split(',');
                            arr_g = CapRas_032_G[y].Split(',');
                            arr_b = CapRas_032_B[y].Split(',');
                            break;
                        case 64:
                            arr_r = CapRas_064_R[y].Split(',');
                            arr_g = CapRas_064_G[y].Split(',');
                            arr_b = CapRas_064_B[y].Split(',');
                            break;
                        case 96:
                            arr_r = CapRas_096_R[y].Split(',');
                            arr_g = CapRas_096_G[y].Split(',');
                            arr_b = CapRas_096_B[y].Split(',');
                            break;
                        case 128:
                            arr_r = CapRas_128_R[y].Split(',');
                            arr_g = CapRas_128_G[y].Split(',');
                            arr_b = CapRas_128_B[y].Split(',');
                            break;
                        case 160:
                            arr_r = CapRas_160_R[y].Split(',');
                            arr_g = CapRas_160_G[y].Split(',');
                            arr_b = CapRas_160_B[y].Split(',');
                            break;
                        case 192:
                            arr_r = CapRas_192_R[y].Split(',');
                            arr_g = CapRas_192_G[y].Split(',');
                            arr_b = CapRas_192_B[y].Split(',');
                            break;
                        case 224:
                            arr_r = CapRas_224_R[y].Split(',');
                            arr_g = CapRas_224_G[y].Split(',');
                            arr_b = CapRas_224_B[y].Split(',');
                            break;
                        case 255:
                            arr_r = CapRas_255_R[y].Split(',');
                            arr_g = CapRas_255_G[y].Split(',');
                            arr_b = CapRas_255_B[y].Split(',');
                            break;
                        default: MessageBox.Show("灰阶不存在！");
                            break;
                    }
                    int x_tep = 0;
                    for (int x = Buf_x0 - 1; x < Buf_x1; x++) //提取特定列
                    {
                        switch (Canvas_Form.Demura_GrayScale[g])
                        {
                            case 32:
                                UDC_CapRas_032_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_032_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_032_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 64:
                                UDC_CapRas_064_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_064_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_064_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 96:
                                UDC_CapRas_096_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_096_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_096_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 128:
                                UDC_CapRas_128_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_128_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_128_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 160:
                                UDC_CapRas_160_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_160_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_160_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 192:
                                UDC_CapRas_192_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_192_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_192_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 224:
                                UDC_CapRas_224_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_224_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_224_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            case 255:
                                UDC_CapRas_255_R[y, x_tep] = double.Parse(arr_r[x]);
                                UDC_CapRas_255_G[y, x_tep] = double.Parse(arr_g[x]);
                                UDC_CapRas_255_B[y, x_tep] = double.Parse(arr_b[x]);
                                break;
                            default:
                                MessageBox.Show("灰阶不存在！");
                                break;
                        }
                        x_tep++;
                    }
                }
            }
            if (Udc_type == 0)//%692UDC
            {
                MASK_692UDC_Region_Data(null, null);
            }
            else if (Udc_type == 1)//%639UDC
            {
                MASK_639UDC_Region_Data(null, null);
            }
             
            Average_UDC_Region_Data(null, null);
        }

        // <summary>
        // 从CSV中提取副屏区域的String值，并存入buf
        // 并根据RGB mask，滤除副屏区buf内的无效数据
        // </summary>
        private void MASK_639UDC_Region_Data(object sender, EventArgs e)
        {
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            int state = 0;//状态机 - MASK映射; Follow Matlab程序 [64*48]
            for (int y = 0; y < 64; y++) //y0~y1
            {
                if (state == 4)
                { state = 1; }
                else
                { state = state + 1; }
                for (int x = 0; x < 48; x++) //x0~x1
                {
                    switch (state)
                    {
                        case 1: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            break;
                        case 2: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            break;
                        case 3: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            break;
                        case 4: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            break;
                        default:
                            state = 0;
                            break;
                    }
                }
            }
        }
        private void MASK_692UDC_Region_Data(object sender, EventArgs e)
        {
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            int state = 0;//状态机 - MASK映射; Follow Matlab程序 [80*48]
            for (int y = 0; y < 80; y++) //y0~y1
            {
                if (state == 4)
                { state = 1; }
                else
                { state = state + 1; }
                for (int x = 0; x < 48; x++) //x0~x1
                {
                    switch (state)
                    {
                        case 1: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            break;
                        case 2: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            break;
                        case 3: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            break;
                        case 4: //行数
                            if (x % 2 == 0)//偶数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                                UDC_CapRas_032_B[y, x] = 0; UDC_CapRas_064_B[y, x] = 0; UDC_CapRas_096_B[y, x] = 0; UDC_CapRas_128_B[y, x] = 0; UDC_CapRas_160_B[y, x] = 0; UDC_CapRas_192_B[y, x] = 0; UDC_CapRas_224_B[y, x] = 0; UDC_CapRas_255_B[y, x] = 0;
                            }
                            else //奇数列
                            {
                                UDC_CapRas_032_R[y, x] = 0; UDC_CapRas_064_R[y, x] = 0; UDC_CapRas_096_R[y, x] = 0; UDC_CapRas_128_R[y, x] = 0; UDC_CapRas_160_R[y, x] = 0; UDC_CapRas_192_R[y, x] = 0; UDC_CapRas_224_R[y, x] = 0; UDC_CapRas_255_R[y, x] = 0;
                                UDC_CapRas_032_G[y, x] = 0; UDC_CapRas_064_G[y, x] = 0; UDC_CapRas_096_G[y, x] = 0; UDC_CapRas_128_G[y, x] = 0; UDC_CapRas_160_G[y, x] = 0; UDC_CapRas_192_G[y, x] = 0; UDC_CapRas_224_G[y, x] = 0; UDC_CapRas_255_G[y, x] = 0;
                            }
                            break;
                        default:
                            state = 0;
                            break;
                    }
                }
            }
        }

        // <summary>
        // 从CSV中提取副屏区域的String值，并存入buf
        // </summary>
        private void Average_UDC_Region_Data(object sender, EventArgs e)
        {
            double sum_r = 0, sum_g = 0, sum_b = 0;
            int count_r = 0, count_g = 0, count_b = 0;
            int udc_area_x = int.Parse(tBx_UDC_x1.Text) - int.Parse(tBx_UDC_x0.Text) + 1; //%%UDC 范围设定
            int udc_area_y = int.Parse(tBx_UDC_y1.Text) - int.Parse(tBx_UDC_y0.Text) + 1;
            double[,] udc_capras_tep_r = new double[udc_area_y, udc_area_x];
            double[,] udc_capras_tep_g = new double[udc_area_y, udc_area_x];
            double[,] udc_capras_tep_b = new double[udc_area_y, udc_area_x];

            for (int g = 0; g < Canvas_Form.Demura_GrayScale.Length; g++)
            { //变更灰阶重置
                sum_r = 0; sum_g = 0; sum_b = 0;
                count_r = 0; count_g = 0; count_b = 0;
                switch (Canvas_Form.Demura_GrayScale[g])
                {
                    case 32:
                        udc_capras_tep_r = UDC_CapRas_032_R;
                        udc_capras_tep_g = UDC_CapRas_032_G;
                        udc_capras_tep_b = UDC_CapRas_032_B;
                        break;
                    case 64:
                        udc_capras_tep_r = UDC_CapRas_064_R;
                        udc_capras_tep_g = UDC_CapRas_064_G;
                        udc_capras_tep_b = UDC_CapRas_064_B;
                        break;
                    case 96:
                        udc_capras_tep_r = UDC_CapRas_096_R;
                        udc_capras_tep_g = UDC_CapRas_096_G;
                        udc_capras_tep_b = UDC_CapRas_096_B;
                        break;
                    case 128:
                        udc_capras_tep_r = UDC_CapRas_128_R;
                        udc_capras_tep_g = UDC_CapRas_128_G;
                        udc_capras_tep_b = UDC_CapRas_128_B;
                        break;
                    case 160:
                        udc_capras_tep_r = UDC_CapRas_160_R;
                        udc_capras_tep_g = UDC_CapRas_160_G;
                        udc_capras_tep_b = UDC_CapRas_160_B;
                        break;
                    case 192:
                        udc_capras_tep_r = UDC_CapRas_192_R;
                        udc_capras_tep_g = UDC_CapRas_192_G;
                        udc_capras_tep_b = UDC_CapRas_192_B;
                        break;
                    case 224:
                        udc_capras_tep_r = UDC_CapRas_224_R;
                        udc_capras_tep_g = UDC_CapRas_224_G;
                        udc_capras_tep_b = UDC_CapRas_224_B;
                        break;
                    case 255:
                        udc_capras_tep_r = UDC_CapRas_255_R;
                        udc_capras_tep_g = UDC_CapRas_255_G;
                        udc_capras_tep_b = UDC_CapRas_255_B;
                        break;
                    default:MessageBox.Show("udc_capras_tep 赋值有误！"); break;
                }
                //%滤除0，转换为一维数组取均值
                foreach (double douArr in udc_capras_tep_r)
                {
                    if (douArr != 0)
                    {
                        sum_r = sum_r + douArr;
                        count_r = count_r + 1;
                    }
                }
                foreach (double douArr in udc_capras_tep_g)
                {
                    if (douArr != 0)
                    {
                        sum_g = sum_g + douArr;
                        count_g = count_g + 1;
                    }
                }
                foreach (double douArr in udc_capras_tep_b)
                {
                    if (douArr != 0)
                    {
                        sum_b = sum_b + douArr;
                        count_b = count_b + 1;
                    }
                }
                Sub_GrayScale_Ave[g,0]= sum_r / count_r;
                Sub_GrayScale_Ave[g,1]= sum_g / count_g;
                Sub_GrayScale_Ave[g,2]= sum_b / count_b;
            }
        }

        // <summary>
        // 从CSV中提取主屏局部区域的String值，并存入buf
        // </summary>
        private void Main_Region_Data_Convert(object sender, EventArgs e)
        {
            string[] arr_r = new string[] { };
            string[] arr_g = new string[] { };
            string[] arr_b = new string[] { };
            int main_area_x=  int.Parse(tBx_Main_x1.Text) - int.Parse(tBx_Main_x0.Text)+1;
            int main_area_y = int.Parse(tBx_Main_y1.Text) - int.Parse(tBx_Main_y0.Text)+1; //%64

            int Main_ave_ragion_X0 = int.Parse(tBx_Main_x0.Text);
            int Main_ave_ragion_X1 = int.Parse(tBx_Main_x1.Text);
            int Main_ave_ragion_Y0 = int.Parse(tBx_Main_y0.Text);
            int Main_ave_ragion_Y1 = int.Parse(tBx_Main_y1.Text);

            double sum_r = 0, sum_g = 0, sum_b = 0;
            int count_r = 0, count_g = 0, count_b = 0;

            double[,] main_capras_tep_r = new double[main_area_y, main_area_x];
            double[,] main_capras_tep_g = new double[main_area_y, main_area_x];
            double[,] main_capras_tep_b = new double[main_area_y, main_area_x];
            //[720*128]

            int y_tep=0;
            //提取各主屏区局部矩阵
            for (int g = 0; g < Canvas_Form.Demura_GrayScale.Length; g++)
            {//提取主屏区局部矩阵
                y_tep = 0;
                for (int y = Main_ave_ragion_Y0; y < Main_ave_ragion_Y1+1; y++) //128行开始
                {   
                    switch (Canvas_Form.Demura_GrayScale[g])
                    {
                        case 32:
                            arr_r = CapRas_032_R[y].Split(',');
                            arr_g = CapRas_032_G[y].Split(',');
                            arr_b = CapRas_032_B[y].Split(',');
                            break;
                        case 64:
                            arr_r = CapRas_064_R[y].Split(',');
                            arr_g = CapRas_064_G[y].Split(',');
                            arr_b = CapRas_064_B[y].Split(',');
                            break;
                        case 96:
                            arr_r = CapRas_096_R[y].Split(',');
                            arr_g = CapRas_096_G[y].Split(',');
                            arr_b = CapRas_096_B[y].Split(',');
                            break;
                        case 128:
                            arr_r = CapRas_128_R[y].Split(',');
                            arr_g = CapRas_128_G[y].Split(',');
                            arr_b = CapRas_128_B[y].Split(',');
                            break;
                        case 160:
                            arr_r = CapRas_160_R[y].Split(',');
                            arr_g = CapRas_160_G[y].Split(',');
                            arr_b = CapRas_160_B[y].Split(',');
                            break;
                        case 192:
                            arr_r = CapRas_192_R[y].Split(',');
                            arr_g = CapRas_192_G[y].Split(',');
                            arr_b = CapRas_192_B[y].Split(',');
                            break;
                        case 224:
                            arr_r = CapRas_224_R[y].Split(',');
                            arr_g = CapRas_224_G[y].Split(',');
                            arr_b = CapRas_224_B[y].Split(',');
                            break;
                        case 255:
                            arr_r = CapRas_255_R[y].Split(',');
                            arr_g = CapRas_255_G[y].Split(',');
                            arr_b = CapRas_255_B[y].Split(',');
                            break;
                        default:
                            MessageBox.Show("主屏-灰阶不存在1！");
                            break;
                    }
                    int x_tep = 0;
                    for (int x = Main_ave_ragion_X0; x < Main_ave_ragion_X1+1; x++)
                    {
                        if ((x >= Buf_x0 - 1 && x < Buf_x1) && (y >= Buf_y0 - 1 && y < Buf_y1))
                        {
                            main_capras_tep_r[y_tep, x_tep] = 0;
                            main_capras_tep_g[y_tep, x_tep] = 0;
                            main_capras_tep_b[y_tep, x_tep] = 0;
                        }
                        else
                        {
                            main_capras_tep_r[y_tep, x_tep] = double.Parse(arr_r[x]);
                            main_capras_tep_g[y_tep, x_tep] = double.Parse(arr_g[x]);
                            main_capras_tep_b[y_tep, x_tep] = double.Parse(arr_b[x]);
                        }
                        x_tep++;
                    }
                    y_tep++;
                }
            
                //Average_Main_Region_Data(null,null);
                sum_r = 0; sum_g = 0; sum_b = 0;
                count_r = 0; count_g = 0; count_b = 0;
                //%滤除0，转换为一维数组取均值
                foreach (double douArr in main_capras_tep_r)
                {
                    sum_r = douArr!=0 ? sum_r + douArr : sum_r;
                    count_r = douArr != 0 ? count_r + 1 : count_r;
                }
                foreach (double douArr in main_capras_tep_g)
                {
                    sum_g = douArr != 0 ? sum_g + douArr : sum_g;
                    count_g = douArr != 0 ? count_g + 1 : count_g;
                }
                foreach (double douArr in main_capras_tep_b)
                {
                    sum_b = douArr != 0 ? sum_b + douArr : sum_b;
                    count_b = douArr != 0 ? count_b + 1 : count_b;
                }
                Main_GrayScale_Ave[g, 0] = sum_r / count_r;
                Main_GrayScale_Ave[g, 1] = sum_g / count_g;
                Main_GrayScale_Ave[g, 2] = sum_b / count_b;
            }
        }
        // <summary>
        // 计算主屏各灰阶的均值
        // </summary>
        private void Average_Main_Region_Data(object sender, EventArgs e)
        {
            double sum_r = 0, sum_g = 0, sum_b = 0;
            int count_r = 0, count_g = 0, count_b = 0;
            int main_active = Buf_hactive; //% 720 --X
            int main_area_y = Buf_y1 - Buf_y0 + 1; //%64 --Y
            main_area_y = main_area_y * 2;
            double[,] main_capras_tep_r = new double[main_area_y, main_active];
            double[,] main_capras_tep_g = new double[main_area_y, main_active];
            double[,] main_capras_tep_b = new double[main_area_y, main_active];

            for (int g = 0; g < Canvas_Form.Demura_GrayScale.Length; g++)
            {
                sum_r = 0; sum_g = 0; sum_b = 0;
                count_r = 0; count_g = 0; count_b = 0;
                switch (Canvas_Form.Demura_GrayScale[g])
                {
                    case 32:
                        main_capras_tep_r = Main_CapRas_032_R;
                        main_capras_tep_g = Main_CapRas_032_G;
                        main_capras_tep_b = Main_CapRas_032_B;
                        break;
                    case 64:
                        main_capras_tep_r = Main_CapRas_064_R;
                        main_capras_tep_g = Main_CapRas_064_G;
                        main_capras_tep_b = Main_CapRas_064_B;
                        break;
                    case 96:
                        main_capras_tep_r = Main_CapRas_096_R;
                        main_capras_tep_g = Main_CapRas_096_G;
                        main_capras_tep_b = Main_CapRas_096_B;
                        break;
                    case 128:
                        main_capras_tep_r = Main_CapRas_128_R;
                        main_capras_tep_g = Main_CapRas_128_G;
                        main_capras_tep_b = Main_CapRas_128_B;
                        break;
                    case 160:
                        main_capras_tep_r = Main_CapRas_160_R;
                        main_capras_tep_g = Main_CapRas_160_G;
                        main_capras_tep_b = Main_CapRas_160_B;
                        break;
                    case 192:
                        main_capras_tep_r = Main_CapRas_192_R;
                        main_capras_tep_g = Main_CapRas_192_G;
                        main_capras_tep_b = Main_CapRas_192_B;
                        break;
                    case 224:
                        main_capras_tep_r = Main_CapRas_224_R;
                        main_capras_tep_g = Main_CapRas_224_G;
                        main_capras_tep_b = Main_CapRas_224_B;
                        break;
                    case 255:
                        main_capras_tep_r = Main_CapRas_255_R;
                        main_capras_tep_g = Main_CapRas_255_G;
                        main_capras_tep_b = Main_CapRas_255_B;
                        break;
                    default: MessageBox.Show("main_capras_tep 赋值有误！"); break;
                }
                //%滤除0，转换为一维数组取均值
                foreach (double douArr in main_capras_tep_r)
                {
                        sum_r = sum_r + douArr;
                        count_r = count_r + 1;
                }
                foreach (double douArr in main_capras_tep_g)
                {
                        sum_g = sum_g + douArr;
                        count_g = count_g + 1;
                }
                foreach (double douArr in main_capras_tep_b)
                {
                        sum_b = sum_b + douArr;
                        count_b = count_b + 1;
                }
                Main_GrayScale_Ave[g, 0] = sum_r / count_r;
                Main_GrayScale_Ave[g, 1] = sum_g / count_g;
                Main_GrayScale_Ave[g, 2] = sum_b / count_b;
            }
        }

        // <summary>
        // 计算主副屏均值的比值的离散程度，并存入Post文件
        // 显示主副屏各自的均值
        // 计算副屏区理想的校准系数
        // </summary>
        private void Diff_Cal_of_UDC_With_Main(object sender, EventArgs e)
        {
            for (int i = 0; i < Num_of_grayscale; i++) //%[32, 64, 96, 128, 160, 192, 224, 255 ]
            {
                Canvas_Form.Diff_R[i] = (double)(Sub_GrayScale_Ave[i, 0] / Main_GrayScale_Ave[i, 0]);//%主副屏均值比值 
                Canvas_Form.Diff_G[i] = (double)(Sub_GrayScale_Ave[i, 1] / Main_GrayScale_Ave[i, 1]);
                Canvas_Form.Diff_B[i] = (double)(Sub_GrayScale_Ave[i, 2] / Main_GrayScale_Ave[i, 2]);

                Post_Form0.Sub_Ave_639UDC_R[i] = (double)(Sub_GrayScale_Ave[i, 0]);
                Post_Form0.Sub_Ave_639UDC_G[i] = (double)(Sub_GrayScale_Ave[i, 1]);
                Post_Form0.Sub_Ave_639UDC_B[i] = (double)(Sub_GrayScale_Ave[i, 2]);

                Post_Form0.Main_Ave_639UDC_R[i] = (double)(Main_GrayScale_Ave[i, 0]);
                Post_Form0.Main_Ave_639UDC_G[i] = (double)(Main_GrayScale_Ave[i, 1]);
                Post_Form0.Main_Ave_639UDC_B[i] = (double)(Main_GrayScale_Ave[i, 2]);
            }
            try
            {
                Post_Form0.Post_Load_FormXml_Update(null, null);
            }
            catch
            { MessageBox.Show("post数据写入失败！"); }
            //------
            //计算主副屏的均值
            for (int i = 0; i < Num_of_grayscale * 3;) //r/g/b*8
            {
                listView_639UDC.Items[i].SubItems[0].Text = Gary_List[i].ToString();
                listView_639UDC.Items[i].SubItems[1].Text = Main_GrayScale_Ave[i/3, 0].ToString();
                listView_639UDC.Items[i].SubItems[2].Text = Sub_GrayScale_Ave[i/3, 0].ToString();

                listView_639UDC.Items[i+1].SubItems[0].Text = Gary_List[i+1].ToString();
                listView_639UDC.Items[i+1].SubItems[1].Text = Main_GrayScale_Ave[i/3, 1].ToString();
                listView_639UDC.Items[i+1].SubItems[2].Text = Sub_GrayScale_Ave[i/3, 1].ToString();

                listView_639UDC.Items[i+2].SubItems[0].Text = Gary_List[i+2].ToString();
                listView_639UDC.Items[i+2].SubItems[1].Text = Main_GrayScale_Ave[i/3, 2].ToString();
                listView_639UDC.Items[i+2].SubItems[2].Text = Sub_GrayScale_Ave[i/3, 2].ToString();
                i = i + 3;
            }
        }

        // <summary>
        // 打印副屏区理想的校准系数
        // </summary>
        private void Btn_CSV_Cal_to_Ref(object sender, EventArgs e)
        {
            Ref_R_639UDC.Text = null;
            Ref_G_639UDC.Text = null;
            Ref_B_639UDC.Text = null;
            for (int i = Num_of_grayscale-1; i >= 0; i--)  //=7
            {
                Ref_R_639UDC.Text += Gary_List[3 * i] + ": " + Canvas_Form.Ref_639UDC_R[i].ToString("0.000") + " ";
                Ref_G_639UDC.Text += Gary_List[3 * i + 1] + ": " + Canvas_Form.Ref_639UDC_G[i].ToString("0.000") + " ";
                Ref_B_639UDC.Text += Gary_List[3 * i + 2] + ": " + Canvas_Form.Ref_639UDC_B[i].ToString("0.000") + " ";
            }
        }

        // <summary>
        // 副屏区RGB各拍摄灰阶的CSV均值与标准gamma2.2对比
        // </summary>
        private void Btn_CSV_Convert_to_Gamma_Click(object sender, EventArgs e)
        {
            try
            {
                UDC_Region_Data_Convert(null, null);
                for (int i = 0; i < Num_of_grayscale; i++) //= 8
                {
                    Gamma_Form.Gamma_R[i] = Sub_GrayScale_Ave[i, 0];
                    Gamma_Form.Gamma_G[i] = Sub_GrayScale_Ave[i, 1];
                    Gamma_Form.Gamma_B[i] = Sub_GrayScale_Ave[i, 2];
                }
            }
            catch
            {
                MessageBox.Show("CSV数据未载入");
            }
            Gamma_Form.ShowDialog();
        }

        private void Btn_Cal_CSV_Click(object sender, EventArgs e)
        {

        }
        // <summary>
        // 分别载入主屏CSV路径
        // </summary>
        public string default0_filePath = "";
        private void Btn_load_main_csv_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.SelectedPath = Default_Path;
            path.ShowDialog();
            txt_path_main.Text = path.SelectedPath;
            default0_filePath = path.SelectedPath;
            defaultfilePath = default0_filePath;
            Default_Path = path.SelectedPath;

            this.listBox_main.Items.Clear();
            string imgtype = "*.CSV";
            string[] ImageType = imgtype.Split('|');
            for (int i = 0; i < ImageType.Length; i++)
            {
                string[] tmp1 = System.IO.Directory.GetFiles(path.SelectedPath, ImageType[i]);
                foreach (string s in tmp1)
                {
                    listBox_main.Items.Add(new FileInfo(s).Name);
                }
            }
        }

        // <summary>
        // 分别载入副屏CSV路径
        // </summary>
        public string default1_filePath = "";
        private void Btn_load_sub_csv_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.SelectedPath = Default_Path;
            path.ShowDialog();
            txt_path_sub.Text = path.SelectedPath;
            default1_filePath = path.SelectedPath;
            Default_Path = path.SelectedPath;

            this.listBox_sub.Items.Clear();
            string imgtype = "*.CSV";
            string[] ImageType = imgtype.Split('|');
            for (int i = 0; i < ImageType.Length; i++)
            {
                string[] tmp1 = System.IO.Directory.GetFiles(path.SelectedPath, ImageType[i]);
                foreach (string s in tmp1)
                {
                    listBox_sub.Items.Add(new FileInfo(s).Name);
                }
            }
        }

        // <summary>
        // 分别载入主屏CSV和副屏CSV,两者合成
        // </summary>
        private void Btn_CSV_Compose_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox_main.Items.Count; i++)
            {
                listBox_main.SelectedIndex = i;
                listBox_sub.SelectedIndex = i;

                try
                {
                    string filePath0 = default0_filePath + "\\" + listBox_main.SelectedItem; //主屏
                    string filePath1 = default1_filePath + "\\" + listBox_sub.SelectedItem; //副屏

                    CSV_data_Compose(filePath0, filePath1);

                    progressBar2.Value = listBox_main.SelectedIndex * 100 / (listBox_main.Items.Count - 1);
                    Percent0.Text = progressBar2.Value.ToString() + "%";
                    Application.DoEvents();
                }
                catch
                {
                    MessageBox.Show("数据加载有误，请检查！");
                }
            }
        }
        // <summary>
        // 写入数据到CSV文件，覆盖形式
        // </summary>
        // <param name="csvPath">要写入的字符串表示的CSV文件</param>
        //副屏区CSV数据逐点乘上校准系数
        private void CSV_data_Compose(string csvPath0, string csvPath1)
        {
            double udc_Tep = 1.0;
            double tep_Diff = 1.0;

            string[] buf_main = new string[] { };
            string[] buf_sub = new string[] { };
            string[] buf_line= new string[Buf_vactive]; //2340  &  2460
            if (this.encoding == null)
            {
                this.encoding = Encoding.Default;
            }
            StreamReader sr_main = new StreamReader(csvPath0, this.encoding); //主屏
            StreamReader sr_sub = new StreamReader(csvPath1, this.encoding); //副屏

            string fileDataLine_Main;
            string fileDataLine_Sub;

            int vcount = 0;
            
            if (listBox_main.SelectedItem.ToString() == "CapRas_032_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_032_R.csv" || listBox_main.SelectedItem.ToString() == "R32.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text); //提取R/G/B离散曲线整体偏移系数
                tep_Diff = Double.Parse(R32_Diff.Text); //提取R/G/B离散曲线具体绑点偏移系数
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_064_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_064_R.csv" || listBox_main.SelectedItem.ToString() == "R64.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R64_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_096_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_096_R.csv" || listBox_main.SelectedItem.ToString() == "R96.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R96_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_128_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_128_R.csv" || listBox_main.SelectedItem.ToString() == "R128.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R128_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_160_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_160_R.csv" || listBox_main.SelectedItem.ToString() == "R160.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R160_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_192_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_192_R.csv" || listBox_main.SelectedItem.ToString() == "R192.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R192_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_224_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_224_R.csv" || listBox_main.SelectedItem.ToString() == "R224.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R224_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_255_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_255_R.csv" || listBox_main.SelectedItem.ToString() == "R255.csv")
            {
                udc_Tep = Double.Parse(UDC_R.Text);
                tep_Diff = Double.Parse(R255_Diff.Text);
            }
            //----------------//-------------------------------------------------------------------------------------------------
            else if (listBox_main.SelectedItem.ToString() == "CapRas_032_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_032_G.csv" || listBox_main.SelectedItem.ToString() == "G32.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G32_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_064_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_064_G.csv" || listBox_main.SelectedItem.ToString() == "G64.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G64_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_096_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_096_G.csv" || listBox_main.SelectedItem.ToString() == "G96.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G96_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_128_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_128_G.csv" || listBox_main.SelectedItem.ToString() == "G128.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G128_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_160_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_160_G.csv" || listBox_main.SelectedItem.ToString() == "G160.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G160_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_192_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_192_G.csv" || listBox_main.SelectedItem.ToString() == "G192.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G192_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_224_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_224_G.csv" || listBox_main.SelectedItem.ToString() == "G224.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G224_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_255_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_255_G.csv" || listBox_main.SelectedItem.ToString() == "G255.csv")
            {
                udc_Tep = Double.Parse(UDC_G.Text);
                tep_Diff = Double.Parse(G255_Diff.Text);
            }
            //----------------//-------------------------------------------------------------------------------------------------
            else if (listBox_main.SelectedItem.ToString() == "CapRas_032_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_032_B.csv" || listBox_main.SelectedItem.ToString() == "B32.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B32_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_064_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_064_B.csv" || listBox_main.SelectedItem.ToString() == "B64.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B64_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_096_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_096_B.csv" || listBox_main.SelectedItem.ToString() == "B96.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B96_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_128_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_128_B.csv" || listBox_main.SelectedItem.ToString() == "B128.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B128_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_160_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_160_B.csv" || listBox_main.SelectedItem.ToString() == "B160.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B160_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_192_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_192_B.csv" || listBox_main.SelectedItem.ToString() == "B192.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B192_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_224_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_224_B.csv" || listBox_main.SelectedItem.ToString() == "B224.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B224_Diff.Text);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_255_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_255_B.csv" || listBox_main.SelectedItem.ToString() == "B255.csv")
            {
                udc_Tep = Double.Parse(UDC_B.Text);
                tep_Diff = Double.Parse(B255_Diff.Text);
            }

            while (vcount< Buf_vactive) //2340 & 2460
            {
                fileDataLine_Main = sr_main.ReadLine();
                fileDataLine_Sub = sr_sub.ReadLine();
                buf_main = fileDataLine_Main.Split(',');
                buf_sub = fileDataLine_Sub.Split(',');

                if (vcount > Buf_y1-1) //仅替换0~63行 / 337~384 列  63&79
                {
                    buf_line[vcount] = fileDataLine_Main;
                }
                else
                {
                    for (int i = Buf_x0 - 1; i <= Buf_x1 - 1; i++)  //336 ~ 383 
                    {
                        buf_main[i] = (Double.Parse(buf_sub[i]) * udc_Tep * tep_Diff).ToString();
                    }
                    int k = 0;
                    foreach (string j in buf_main)
                    {
                        buf_line[vcount] = buf_line[vcount] + j;
                        if (k < Buf_hactive-1)  //719
                        {
                            buf_line[vcount] = buf_line[vcount] + ',';
                        }
                        k++;
                    }
                }
                vcount++;
            }
            sr_main.Close();
            sr_sub.Close();
            //----------------------------主屏CSV写----------------------
            StreamWriter sw = new StreamWriter(csvPath0, false, Encoding.Default);
            //System.IO.File.WriteAllLines(csvPath0, buf_line, Encoding.UTF8);
            for (int i = 0; i < Buf_vactive; i++)   //2340 &2460
            {
                sw.WriteLine(buf_line[i]);
            }
            sw.Close();
        }

        private void ToolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void 更新限度样ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Canvas_Form.Load_FormXml_Update(null, null);
            }
            catch
            {
                MessageBox.Show("基准值-更新失败！");
            }
            
        }

        private void 还原默认值ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Canvas_Form.Load_FormXml_Return_to_Original(Magic_R0.Text, Magic_G0.Text, Magic_B0.Text);
                MessageBox.Show("已还原出厂设置！");
            }
            catch
            {
                MessageBox.Show("还原失败！");
            }
        }

        private void 保存配置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            byte[] InitiateFile = new byte[78];
            Int16 tep = 0;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            byte[] FileHead = System.Text.Encoding.Default.GetBytes("Visionox_Config.CFG");
            byte[] FileEnd = System.Text.Encoding.Default.GetBytes("Visionox_CFGEND");
            saveFileDialog1.FileName = "Visionox_Config.CFG";
            saveFileDialog1.Filter = "Visionox_ Configure File(*.cfg)|*.cfg";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();//输出文件
                Array.Clear(InitiateFile, 0, 78);
                Array.Copy(FileHead, InitiateFile, FileHead.Length);
                tep = Convert.ToInt16(Double.Parse(UDC_R.Text)*1000);
                InitiateFile[0] = (byte)(tep >> 8);
                InitiateFile[1] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(UDC_G.Text) * 1000);
                InitiateFile[2] = (byte)(tep >> 8);
                InitiateFile[3] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(UDC_B.Text) * 1000);
                InitiateFile[4] = (byte)(tep >> 8);
                InitiateFile[5] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R255_Diff.Text) * 1000);
                InitiateFile[6] = (byte)(tep >> 8);
                InitiateFile[7] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R224_Diff.Text) * 1000);
                InitiateFile[8] = (byte)(tep >> 8);
                InitiateFile[9] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R192_Diff.Text) * 1000);
                InitiateFile[10] = (byte)(tep >> 8);
                InitiateFile[11] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R160_Diff.Text) * 1000);
                InitiateFile[12] = (byte)(tep >> 8);
                InitiateFile[13] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R128_Diff.Text) * 1000);
                InitiateFile[14] = (byte)(tep >> 8);
                InitiateFile[15] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R96_Diff.Text) * 1000);
                InitiateFile[16] = (byte)(tep >> 8);
                InitiateFile[17] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R64_Diff.Text) * 1000);
                InitiateFile[18] = (byte)(tep >> 8);
                InitiateFile[19] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(R32_Diff.Text) * 1000);
                InitiateFile[20] = (byte)(tep >> 8);
                InitiateFile[21] = (byte)(tep);
                //--------------------------
                tep = Convert.ToInt16(Double.Parse(G255_Diff.Text) * 1000);
                InitiateFile[22] = (byte)(tep >> 8);
                InitiateFile[23] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G224_Diff.Text) * 1000);
                InitiateFile[24] = (byte)(tep >> 8);
                InitiateFile[25] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G192_Diff.Text) * 1000);
                InitiateFile[26] = (byte)(tep >> 8);
                InitiateFile[27] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G160_Diff.Text) * 1000);
                InitiateFile[28] = (byte)(tep >> 8);
                InitiateFile[29] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G128_Diff.Text) * 1000);
                InitiateFile[30] = (byte)(tep >> 8);
                InitiateFile[31] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G96_Diff.Text) * 1000);
                InitiateFile[32] = (byte)(tep >> 8);
                InitiateFile[33] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G64_Diff.Text) * 1000);
                InitiateFile[34] = (byte)(tep >> 8);
                InitiateFile[35] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(G32_Diff.Text) * 1000);
                InitiateFile[36] = (byte)(tep >> 8);
                InitiateFile[37] = (byte)(tep);
                //--------------------------
                tep = Convert.ToInt16(Double.Parse(B255_Diff.Text) * 1000);
                InitiateFile[38] = (byte)(tep >> 8);
                InitiateFile[39] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B224_Diff.Text) * 1000);
                InitiateFile[40] = (byte)(tep >> 8);
                InitiateFile[41] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B192_Diff.Text) * 1000);
                InitiateFile[42] = (byte)(tep >> 8);
                InitiateFile[43] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B160_Diff.Text) * 1000);
                InitiateFile[44] = (byte)(tep >> 8);
                InitiateFile[45] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B128_Diff.Text) * 1000);
                InitiateFile[46] = (byte)(tep >> 8);
                InitiateFile[47] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B96_Diff.Text) * 1000);
                InitiateFile[48] = (byte)(tep >> 8);
                InitiateFile[49] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B64_Diff.Text) * 1000);
                InitiateFile[50] = (byte)(tep >> 8);
                InitiateFile[51] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(B32_Diff.Text) * 1000);
                InitiateFile[52] = (byte)(tep >> 8);
                InitiateFile[53] = (byte)(tep);
                //-----------------------------------------------------------------------------
                tep = Convert.ToInt16(Double.Parse(Magic_R0.Text) * 1000);
                InitiateFile[54] = (byte)(tep >> 8);
                InitiateFile[55] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_G0.Text) * 1000);
                InitiateFile[56] = (byte)(tep >> 8);
                InitiateFile[57] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_B0.Text) * 1000);
                InitiateFile[58] = (byte)(tep >> 8);
                InitiateFile[59] = (byte)(tep);

                tep = Convert.ToInt16(Double.Parse(Magic_R1.Text) * 1000);
                InitiateFile[60] = (byte)(tep >> 8);
                InitiateFile[61] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_G1.Text) * 1000);
                InitiateFile[62] = (byte)(tep >> 8);
                InitiateFile[63] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_B1.Text) * 1000);
                InitiateFile[64] = (byte)(tep >> 8);
                InitiateFile[65] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_R2.Text) * 1000);
                InitiateFile[66] = (byte)(tep >> 8);
                InitiateFile[67] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_G2.Text) * 1000);
                InitiateFile[68] = (byte)(tep >> 8);
                InitiateFile[69] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_B2.Text) * 1000);
                InitiateFile[70] = (byte)(tep >> 8);
                InitiateFile[71] = (byte)(tep);

                tep = Convert.ToInt16(Double.Parse(Magic_R0_degree.Text) * 1000);
                InitiateFile[72] = (byte)(tep >> 8);
                InitiateFile[73] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_G0_degree.Text) * 1000);
                InitiateFile[74] = (byte)(tep >> 8);
                InitiateFile[75] = (byte)(tep);
                tep = Convert.ToInt16(Double.Parse(Magic_B0_degree.Text) * 1000);
                InitiateFile[76] = (byte)(tep >> 8);
                InitiateFile[77] = (byte)(tep);

                fs.Write(InitiateFile, 0, 78);       //0x00-0x1F:文件头，"GVOConfi1.CFG"
                fs.Close();


            }
        }
        private void 载入配置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            byte[] TemFileContent = new byte[100];
            double tep = 0;
            System.Int32 ByteReaded = 0x00;
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = @"cfg文件(*.cfg)|*.cfg|所有文件(*.*)|*.*"; ;
            of.Title = "载入配置文件";

            if (of.ShowDialog() == DialogResult.OK)
            {
                //读取文本信息
                FileStream fs = new FileStream(of.FileName, FileMode.Open, FileAccess.Read);
                fs.Seek(0, SeekOrigin.Begin);
                ByteReaded = fs.Read(TemFileContent, 0, 78);//读取文件从地址0的512个数据
                tep = (double)((TemFileContent[0] << 8) + TemFileContent[1])/1000;
                UDC_R.Text = tep.ToString();
                tep = (double)((TemFileContent[2] << 8) + TemFileContent[3]) / 1000;
                UDC_G.Text = tep.ToString();
                tep = (double)((TemFileContent[4] << 8) + TemFileContent[5]) / 1000;
                UDC_B.Text = tep.ToString();
                //--------------------------
                tep = (double)((TemFileContent[6] << 8) + TemFileContent[7]) / 1000;
                R255_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[8] << 8) + TemFileContent[9]) / 1000;
                R224_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[10] << 8) + TemFileContent[11]) / 1000;
                R192_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[12] << 8) + TemFileContent[13]) / 1000;
                R160_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[14] << 8) + TemFileContent[15]) / 1000;
                R128_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[16] << 8) + TemFileContent[17]) / 1000;
                R96_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[18] << 8) + TemFileContent[19]) / 1000;
                R64_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[20] << 8) + TemFileContent[21]) / 1000;
                R32_Diff.Text = tep.ToString();
                //--------------------------
                tep = (double)((TemFileContent[22] << 8) + TemFileContent[23]) / 1000;
                G255_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[24] << 8) + TemFileContent[25]) / 1000;
                G224_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[26] << 8) + TemFileContent[27]) / 1000;
                G192_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[28] << 8) + TemFileContent[29]) / 1000;
                G160_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[30] << 8) + TemFileContent[31]) / 1000;
                G128_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[32] << 8) + TemFileContent[33]) / 1000;
                G96_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[34] << 8) + TemFileContent[35]) / 1000;
                G64_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[36] << 8) + TemFileContent[37]) / 1000;
                G32_Diff.Text = tep.ToString();
                //--------------------------
                tep = (double)((TemFileContent[38] << 8) + TemFileContent[39]) / 1000;
                B255_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[40] << 8) + TemFileContent[41]) / 1000;
                B224_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[42] << 8) + TemFileContent[43]) / 1000;
                B192_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[44] << 8) + TemFileContent[45]) / 1000;
                B160_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[46] << 8) + TemFileContent[47]) / 1000;
                B128_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[48] << 8) + TemFileContent[49]) / 1000;
                B96_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[50] << 8) + TemFileContent[51]) / 1000;
                B64_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[52] << 8) + TemFileContent[53]) / 1000;
                B32_Diff.Text = tep.ToString();
                tep = (double)((TemFileContent[54] << 8) + TemFileContent[55]) / 1000;
                //-----------------------------------------
                Magic_R0.Text = tep.ToString();
                tep = (double)((TemFileContent[56] << 8) + TemFileContent[57]) / 1000;
                Magic_G0.Text = tep.ToString();
                tep = (double)((TemFileContent[58] << 8) + TemFileContent[59]) / 1000;
                Magic_B0.Text = tep.ToString();
                tep = (double)((TemFileContent[60] << 8) + TemFileContent[61]) / 1000;
                Magic_R1.Text = tep.ToString();
                tep = (double)((TemFileContent[62] << 8) + TemFileContent[63]) / 1000;
                Magic_G1.Text = tep.ToString();
                tep = (double)((TemFileContent[64] << 8) + TemFileContent[65]) / 1000;
                Magic_B1.Text = tep.ToString();
                tep = (double)((TemFileContent[66] << 8) + TemFileContent[67]) / 1000;
                Magic_R2.Text = tep.ToString();
                tep = (double)((TemFileContent[68] << 8) + TemFileContent[69]) / 1000;
                Magic_G2.Text = tep.ToString();
                tep = (double)((TemFileContent[70] << 8) + TemFileContent[71]) / 1000;
                Magic_B2.Text = tep.ToString();
                tep = (double)((TemFileContent[72] << 8) + TemFileContent[73]) / 1000;
                Magic_R0_degree.Text = tep.ToString();
                tep = (double)((TemFileContent[74] << 8) + TemFileContent[75]) / 1000;
                Magic_G0_degree.Text = tep.ToString();
                tep = (double)((TemFileContent[76] << 8) + TemFileContent[77]) / 1000;
                Magic_B0_degree.Text = tep.ToString();
                fs.Close();
            }
        }

        private void Btn_Diff_Reset_Click(object sender, EventArgs e)
        {
            UDC_R.Text = "1.0"; UDC_G.Text = "1.0"; UDC_B.Text = "1.0";
            R255_Diff.Text = "1.0"; G255_Diff.Text = "1.0"; B255_Diff.Text = "1.0";
            R224_Diff.Text = "1.0"; G224_Diff.Text = "1.0"; B224_Diff.Text = "1.0";
            R192_Diff.Text = "1.0"; G192_Diff.Text = "1.0"; B192_Diff.Text = "1.0";
            R160_Diff.Text = "1.0"; G160_Diff.Text = "1.0"; B160_Diff.Text = "1.0";
            R128_Diff.Text = "1.0"; G128_Diff.Text = "1.0"; B128_Diff.Text = "1.0";
            R96_Diff.Text = "1.0";  G96_Diff.Text = "1.0";  B96_Diff.Text = "1.0";
            R64_Diff.Text = "1.0";  G64_Diff.Text = "1.0";  B64_Diff.Text = "1.0";
            R32_Diff.Text = "1.0";  G32_Diff.Text = "1.0";  B32_Diff.Text = "1.0";
        }
        private void Btn_Diff_Ref_Load_Click(object sender, EventArgs e)
        {
            UDC_R.Text = "1.0"; UDC_G.Text = "1.0"; UDC_B.Text = "1.0";
            R255_Diff.Text = Canvas_Form.Ref_639UDC_R[7].ToString("0.000");  G255_Diff.Text = Canvas_Form.Ref_639UDC_G[7].ToString("0.000"); B255_Diff.Text = Canvas_Form.Ref_639UDC_B[7].ToString("0.000");
            R224_Diff.Text = Canvas_Form.Ref_639UDC_R[6].ToString("0.000");  G224_Diff.Text = Canvas_Form.Ref_639UDC_G[6].ToString("0.000"); B224_Diff.Text = Canvas_Form.Ref_639UDC_B[6].ToString("0.000");
            R192_Diff.Text = Canvas_Form.Ref_639UDC_R[5].ToString("0.000");  G192_Diff.Text = Canvas_Form.Ref_639UDC_G[5].ToString("0.000"); B192_Diff.Text = Canvas_Form.Ref_639UDC_B[5].ToString("0.000");
            R160_Diff.Text = Canvas_Form.Ref_639UDC_R[4].ToString("0.000");  G160_Diff.Text = Canvas_Form.Ref_639UDC_G[4].ToString("0.000"); B160_Diff.Text = Canvas_Form.Ref_639UDC_B[4].ToString("0.000");
            R128_Diff.Text = Canvas_Form.Ref_639UDC_R[3].ToString("0.000");  G128_Diff.Text = Canvas_Form.Ref_639UDC_G[3].ToString("0.000"); B128_Diff.Text = Canvas_Form.Ref_639UDC_B[3].ToString("0.000");
            R96_Diff.Text =  Canvas_Form.Ref_639UDC_R[2].ToString("0.000");  G96_Diff.Text = Canvas_Form.Ref_639UDC_G[2].ToString("0.000"); B96_Diff.Text = Canvas_Form.Ref_639UDC_B[2].ToString("0.000");
            R64_Diff.Text =  Canvas_Form.Ref_639UDC_R[1].ToString("0.000");  G64_Diff.Text = Canvas_Form.Ref_639UDC_G[1].ToString("0.000"); B64_Diff.Text = Canvas_Form.Ref_639UDC_B[1].ToString("0.000");
            R32_Diff.Text =  Canvas_Form.Ref_639UDC_R[0].ToString("0.000");  G32_Diff.Text = Canvas_Form.Ref_639UDC_G[0].ToString("0.000"); B32_Diff.Text = Canvas_Form.Ref_639UDC_B[0].ToString("0.000");
        }
        private void ListView_639UDC_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Post_Form0.ShowDialog();
        }

        private void Btn_CSV_to_Line_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox_main.Items.Count; i++)
            {
                listBox_main.SelectedIndex = i;
                try
                {
                    string filePath0 = default0_filePath + "\\" + listBox_main.SelectedItem; //主屏
                    CSV_data_Stretch(filePath0);

                    progressBar2.Value = listBox_main.SelectedIndex * 100 / (listBox_main.Items.Count - 1);
                    Percent0.Text = progressBar2.Value.ToString() + "%";
                    Application.DoEvents();
                }
                catch
                {
                    MessageBox.Show("数据加载有误，请检查！");
                }
            }
        }

        private void UDC_da_wei_tian_long(double diff, double target_x, double degree,out double udc_ideal)
        {
            double interpose_region = double.Parse(Magic_region.Text);
            double Target_ideal = 1.0;
            udc_ideal = 1.0;
            //在r_target_64_32±r_degree表示需要干预
            if (diff > target_x + degree || diff < target_x - degree || cBx_All_intervene.Checked == true)
            {
                //r_target_64_32+r_degree %目标最大值，干预范围 r_target_64_32±r_degree； //r_degree≈0.2
                //Canvas_Form.Diff_R[0] %实际x主副差值，干预范围 r_target_64_32±interpose_region； //interpose_region ≈±0.5
                //Step实际&step理想= Interpose_region / degree 倍关系；
                //Step实际=abs(r_target_64_32 + interpose_region - Canvas_Form.Diff_R[0]);
                //step理想=abs(r_target_64_32 + interpose_region - Canvas_Form.Diff_R[0])/ (interpose_region / degree);
                //Target干预值= r_target_64_32+ r_degree - step理想；
                if ((target_x + interpose_region) < diff)
                {
                    Target_ideal = target_x + degree;
                }
                else if ((target_x - interpose_region) > diff)
                {
                    Target_ideal = target_x - degree;
                }
                else
                {
                    Target_ideal = target_x + degree - (double)(Math.Abs(target_x + interpose_region - diff) / (double)(interpose_region / degree));
                }
                udc_ideal = (double)(Target_ideal / diff);//提取R/G/B离散曲线整体偏移系数
            }
        }

        // <summary>
        // 黑科技，拉直CSV离散曲线到定值
        // </summary>
        // <param name="csvPath">要写入的字符串表示的CSV文件</param>
        //副屏区CSV数据逐点乘上校准系数
        private void CSV_data_Stretch(string csvPath0)
        {
            double udc_Tep=1.0; //默认不干预；
            double Target_ideal=1.0;
            double r_target_64_32 = double.Parse(Magic_R0.Text); 
            double g_target_64_32 = double.Parse(Magic_G0.Text);
            double b_target_64_32 = double.Parse(Magic_B0.Text); //64~32
            double r_target_192_96 = double.Parse(Magic_R1.Text);
            double g_target_192_96 = double.Parse(Magic_G1.Text);
            double b_target_192_96 = double.Parse(Magic_B1.Text); //192~64
            double r_target_255_224 = double.Parse(Magic_R2.Text);
            double g_target_255_224 = double.Parse(Magic_G2.Text);
            double b_target_255_224 = double.Parse(Magic_B2.Text); //255~192
            double r_degree = double.Parse(Magic_R0_degree.Text);
            double g_degree = double.Parse(Magic_G0_degree.Text);
            double b_degree = double.Parse(Magic_B0_degree.Text);

            string[] buf_main = new string[] { };
            string[] buf_sub = new string[] { };
            string[] buf_line = new string[Buf_vactive]; //2340  &  2460
            if (this.encoding == null)
            {
                this.encoding = Encoding.Default;
            }
            StreamReader sr_main = new StreamReader(csvPath0, this.encoding); //主屏

            string fileDataLine_Main;

            int vcount = 0;
            //%[32, 64, 96, 128, 160, 192, 224, 255 ]
            if (listBox_main.SelectedItem.ToString() == "CapRas_032_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_032_R.csv" || listBox_main.SelectedItem.ToString() == "R32.csv")
            {
                /*
                //在r_target_64_32±r_degree表示需要干预
                if (Canvas_Form.Diff_R[0] > r_target_64_32 + r_degree || Canvas_Form.Diff_R[0] < r_target_64_32 - r_degree)
                {
                    //r_target_64_32+r_degree %目标最大值，干预范围 r_target_64_32±r_degree； //r_degree≈0.2
                    //Canvas_Form.Diff_R[0] %实际x主副差值，干预范围 r_target_64_32±1；
                    //Step实际&step理想=5倍关系；
                    //Step实际=abs(r_target_64_32+1-Canvas_Form.Diff_R[0]);
                    //step理想=abs(r_target_64_32+1-Canvas_Form.Diff_R[0])/5;
                    //Target干预值= r_target_64_32+0.2-step理想；
                    if ((r_target_64_32 + 1) < Canvas_Form.Diff_R[0])
                    {
                        Target_ideal = r_target_64_32 + r_degree;
                    }
                    else if ((r_target_64_32 - 1) > Canvas_Form.Diff_R[0])
                    {
                        Target_ideal = r_target_64_32 - r_degree;
                    }
                    else
                    {
                        Target_ideal = r_target_64_32 + r_degree - (Math.Abs(r_target_64_32 + 1 - Canvas_Form.Diff_R[0]) / 5);
                    }
                    udc_Tep = (double)(Target_ideal / Canvas_Form.Diff_R[0]);//提取R/G/B离散曲线整体偏移系数
                }*/
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[0], r_target_64_32, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_064_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_064_R.csv" || listBox_main.SelectedItem.ToString() == "R64.csv")
            {
                /*
                if (Canvas_Form.Diff_R[1] > r_target_64_32 + r_degree || Canvas_Form.Diff_R[1] < r_target_64_32 - r_degree)
                {
                    udc_Tep = (double)(r_target_64_32 / Canvas_Form.Diff_R[1]);
                }*/
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[1], r_target_64_32, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_096_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_096_R.csv" || listBox_main.SelectedItem.ToString() == "R96.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[2], r_target_192_96, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_128_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_128_R.csv" || listBox_main.SelectedItem.ToString() == "R128.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[3], r_target_192_96, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_160_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_160_R.csv" || listBox_main.SelectedItem.ToString() == "R160.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[4], r_target_192_96, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_192_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_192_R.csv" || listBox_main.SelectedItem.ToString() == "R192.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[5], r_target_192_96, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_224_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_224_R.csv" || listBox_main.SelectedItem.ToString() == "R224.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[6], r_target_255_224, r_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_255_R.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_255_R.csv" || listBox_main.SelectedItem.ToString() == "R255.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_R[7], r_target_255_224, r_degree, out udc_Tep);
            }
            //----------------//-------------------------------------------------------------------------------------------------
            else if (listBox_main.SelectedItem.ToString() == "CapRas_032_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_032_G.csv" || listBox_main.SelectedItem.ToString() == "G32.csv")
            {/*
                if (Canvas_Form.Diff_G[0] > g_target_64_32 + g_degree || Canvas_Form.Diff_G[0] < g_target_64_32 - g_degree)
                {
                    udc_Tep = (double)(g_target_64_32 / Canvas_Form.Diff_G[0]);
                }*/
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[0], g_target_64_32, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_064_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_064_G.csv" || listBox_main.SelectedItem.ToString() == "G64.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[1], g_target_64_32, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_096_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_096_G.csv" || listBox_main.SelectedItem.ToString() == "G96.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[2], g_target_192_96, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_128_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_128_G.csv" || listBox_main.SelectedItem.ToString() == "G128.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[3], g_target_192_96, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_160_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_160_G.csv" || listBox_main.SelectedItem.ToString() == "G160.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[4], g_target_192_96, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_192_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_192_G.csv" || listBox_main.SelectedItem.ToString() == "G192.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[5], g_target_192_96, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_224_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_224_G.csv" || listBox_main.SelectedItem.ToString() == "G224.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[6], g_target_255_224, g_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_255_G.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_255_G.csv" || listBox_main.SelectedItem.ToString() == "G255.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_G[7], g_target_255_224, g_degree, out udc_Tep);
            }
            //----------------//-------------------------------------------------------------------------------------------------
            else if (listBox_main.SelectedItem.ToString() == "CapRas_032_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_032_B.csv" || listBox_main.SelectedItem.ToString() == "B32.csv")
            {/*
                if (Canvas_Form.Diff_B[0] > b_target_64_32 + b_degree || Canvas_Form.Diff_B[0] < b_target_64_32 - b_degree)
                {
                    udc_Tep = (double)(b_target_64_32 / Canvas_Form.Diff_B[0]);
                }*/
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[0], b_target_64_32, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_064_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_064_B.csv" || listBox_main.SelectedItem.ToString() == "B64.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[1], b_target_64_32, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_096_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_096_B.csv" || listBox_main.SelectedItem.ToString() == "B96.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[2], b_target_192_96, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_128_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_128_B.csv" || listBox_main.SelectedItem.ToString() == "B128.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[3], b_target_192_96, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_160_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_160_B.csv" || listBox_main.SelectedItem.ToString() == "B160.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[4], b_target_192_96, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_192_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_192_B.csv" || listBox_main.SelectedItem.ToString() == "B192.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[5], b_target_192_96, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_224_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_224_B.csv" || listBox_main.SelectedItem.ToString() == "B224.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[6], b_target_255_224, b_degree, out udc_Tep);
            }
            else if (listBox_main.SelectedItem.ToString() == "CapRas_255_B.CSV" || listBox_main.SelectedItem.ToString() == "CapRas_255_B.csv" || listBox_main.SelectedItem.ToString() == "B255.csv")
            {
                UDC_da_wei_tian_long(Canvas_Form.Diff_B[7], b_target_255_224, b_degree, out udc_Tep);
            }

            while (vcount < Buf_vactive) //2340 & 2460
            {
                fileDataLine_Main = sr_main.ReadLine();
                buf_main = fileDataLine_Main.Split(',');

                if (vcount > Buf_y1 - 1) //仅替换0~63行 / 337~384 列  63&79
                {
                    buf_line[vcount] = fileDataLine_Main;
                }
                else
                {
                    for (int i = Buf_x0 - 1; i <= Buf_x1 - 1; i++)  //336 ~ 383 
                    {
                        buf_main[i] = (Double.Parse(buf_main[i]) * udc_Tep).ToString();
                    }
                    int k = 0;
                    foreach (string j in buf_main)
                    {
                        buf_line[vcount] = buf_line[vcount] + j;
                        if (k < Buf_hactive - 1)  //719
                        {
                            buf_line[vcount] = buf_line[vcount] + ',';
                        }
                        k++;
                    }
                }
                vcount++;
            }
            sr_main.Close();
            //----------------------------主屏CSV写----------------------
            StreamWriter sw = new StreamWriter(csvPath0, false, Encoding.Default);
            //System.IO.File.WriteAllLines(csvPath0, buf_line, Encoding.UTF8);
            for (int i = 0; i < Buf_vactive; i++)   //2340 &2460
            {
                sw.WriteLine(buf_line[i]);
            }
            sw.Close();
        }

        private void Btn_Diff_Saveout_Click(object sender, EventArgs e)
        {
            string Diff_string = null;
            Diff_string += "校准系数" + "\r\n";
            Diff_string += "UDC_R/G/B: " + UDC_R.Text + "," + UDC_G.Text + "," + UDC_B.Text + "\r\n";
            Diff_string += "R_line：" + R32_Diff.Text + "," + R64_Diff.Text + "," + R96_Diff.Text + "," + R128_Diff.Text + "," + R160_Diff.Text + "," + R192_Diff.Text + "," + R224_Diff.Text + "," + R255_Diff.Text + "\r\n";
            Diff_string += "G_line：" + G32_Diff.Text + "," + G64_Diff.Text + "," + G96_Diff.Text + "," + G128_Diff.Text + "," + G160_Diff.Text + "," + G192_Diff.Text + "," + G224_Diff.Text + "," + G255_Diff.Text + "\r\n";
            Diff_string += "B_line：" + B32_Diff.Text + "," + B64_Diff.Text + "," + B96_Diff.Text + "," + B128_Diff.Text + "," + B160_Diff.Text + "," + B192_Diff.Text + "," + B224_Diff.Text + "," + B255_Diff.Text + "\r\n";

            //创建保存对话框对象
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Code files (*.txt)|*.txt"; //过滤文件类型
            save.FilterIndex = 1;
            save.RestoreDirectory = true;
            if (save.ShowDialog() == DialogResult.OK)
            {
                string path = save.FileName;
                save.DefaultExt = ".txt";
                StreamWriter sw = new StreamWriter(path);
                sw.WriteLine(Diff_string);
                sw.Close();
            }

        }

        private void ListView_639UDC_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ListView_Printf_Click(object sender, EventArgs e)
        {
            string Diff_string = null;
            Diff_string += "主副屏数据统计:" + "\r\n";
            Diff_string += "对象 ， 主屏 ， 副屏 " + "\r\n";
            for (int i = 0; i < Num_of_grayscale * 3;) //r/g/b*8
            {
                Diff_string += listView_639UDC.Items[i].SubItems[0].Text + ",       " + listView_639UDC.Items[i].SubItems[1].Text + ",      " + listView_639UDC.Items[i].SubItems[2].Text + "\r\n";
                Diff_string += listView_639UDC.Items[i + 1].SubItems[0].Text + ",       " + listView_639UDC.Items[i + 1].SubItems[1].Text + ",      " + listView_639UDC.Items[i + 1].SubItems[2].Text + "\r\n";
                Diff_string += listView_639UDC.Items[i + 2].SubItems[0].Text + ",       " + listView_639UDC.Items[i + 2].SubItems[1].Text + ",      " + listView_639UDC.Items[i + 2].SubItems[2].Text + "\r\n";
                i = i + 3;
            }

            //创建保存对话框对象
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Code files (*.txt)|*.txt"; //过滤文件类型
            save.FilterIndex = 1;
            save.RestoreDirectory = true;
            if (save.ShowDialog() == DialogResult.OK)
            {
                string path = save.FileName;
                save.DefaultExt = ".txt";
                StreamWriter sw = new StreamWriter(path);
                sw.WriteLine(Diff_string);
                sw.Close();
            }
        }
    }
}
